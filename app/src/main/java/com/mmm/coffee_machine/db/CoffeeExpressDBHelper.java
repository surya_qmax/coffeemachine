package com.mmm.coffee_machine.db;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mmm.coffee_machine.Common;


public class CoffeeExpressDBHelper extends SQLiteOpenHelper {

    static String TAG = "CoffeeExpressDBHelper.java";
    private static CoffeeExpressDBHelper mInstance = null;

    public CoffeeExpressDBHelper(Context context, String name,
                                 SQLiteDatabase.CursorFactory factory, int version,
                                 DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public static CoffeeExpressDBHelper getInstance(Context context) {
        //Create database
        if (mInstance == null) {
            mInstance = new CoffeeExpressDBHelper(context, Common.DB_NAME, null, Common.DB_VERSION, null);
            Log.d(TAG, "New Database Object Created");
        } else {
            Log.d(TAG, "Instance not null,Database object already created");
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Create all table
        CoffeeExpressTable coffeeExpress_table = new CoffeeExpressTable();
        coffeeExpress_table.createDB(sqLiteDatabase);

        serialNoTable serialNoTable = new serialNoTable();
        serialNoTable.createDB(sqLiteDatabase);
    }



    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        //Upgrade all table
        if (oldVersion < newVersion) {

            CoffeeExpressTable coffeeExpress_table = new CoffeeExpressTable();
            coffeeExpress_table.upgradeDB(sqLiteDatabase);

            serialNoTable serialNoTable = new serialNoTable();
            serialNoTable.upgradeDB(sqLiteDatabase);


            Log.d(TAG, "New Version updated");
        } else
            Log.d(TAG, "Version already updated");
    }
}

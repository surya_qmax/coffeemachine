package com.mmm.coffee_machine.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by MMM on 8/12/2016.
 */

public class CoffeeExpressTable extends ContentProvider {

    public static final String _ID = "_id";

    public static final String DATE = "date";
    public static final String BCCMSB = "bcc_msb";
    public static final String SCCMSB = "scc_msb";
    public static final String LCCMSB = "lcc_msb";
    public static final String BTCMSB = "btc_msb";
    public static final String STCMSB = "stc_msb";
    public static final String LTCMSB = "ltc_msb";
    public static final String HMCMSB = "hmc_msb";

    public static final String BCCLSB = "bcc_lsb";
    public static final String SCCLSB = "scc_lsb";
    public static final String LCCLSB = "lcc_lsb";
    public static final String BTCLSB = "btc_lsb";
    public static final String STCLSB = "stc_lsb";
    public static final String LTCLSB = "ltc_lsb";
    public static final String HMCLSB = "hmc_lsb";



    // fields for my content provider
    public static final String PROVIDER_NAME = "com.mmm.coffee_machine.provider.status";
    public static final String URL = "content://" + PROVIDER_NAME + "/coffee_express_table";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    public static final String TABLE_NAME = "coffee_express_table";

    private static String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "( " +
                    _ID + " integer primary key autoincrement , " +
                    DATE + " DATE," +
                    BCCMSB + " VARCHAR," +
                    BCCLSB + " VARCHAR," +
                    SCCMSB + " VARCHAR," +
                    SCCLSB + " VARCHAR," +
                    LCCMSB + " VARCHAR," +
                    LCCLSB + " VARCHAR," +
                    BTCMSB + " VARCHAR," +
                    BTCLSB + " VARCHAR," +
                    STCMSB + " VARCHAR," +
                    STCLSB + " VARCHAR," +
                    LTCMSB + " VARCHAR," +
                    LTCLSB + " VARCHAR," +
                    HMCMSB + " VARCHAR," +
                    HMCLSB + " VARCHAR)";

    // mDataBase declarations
    private SQLiteDatabase mDataBase;

    public CoffeeExpressTable() {
    }

    public void createDB(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    public void upgradeDB(SQLiteDatabase sqLiteDatabase) {
        String GENERAL_DROP_TABLE = "DROP TABLE" + TABLE_NAME;
        sqLiteDatabase.execSQL(GENERAL_DROP_TABLE);
        createDB(sqLiteDatabase);
    }

    @Override
    public boolean onCreate() {
        // permissions to be writable
        mDataBase = CoffeeExpressDBHelper.getInstance(getContext()).getWritableDatabase();
        return mDataBase != null;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // the TABLE_NAME to query on
        queryBuilder.setTables(TABLE_NAME);
        Cursor cursor = queryBuilder.query(mDataBase, projection, selection,
                selectionArgs, null, null, null);
        /**
         * register to watch a content URI for changes
         */
        if (getContext() != null)
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long row = mDataBase.insert(TABLE_NAME, "", values);

        // If record is added successfully
        if (row > 0) {
            Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
            if (getContext() != null)
                getContext().getContentResolver().notifyChange(newUri, null);
            Log.v("mmm", "location added");
            return newUri;
        }
        throw new SQLException("Fail to add a new record into " + uri);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count;
        count = mDataBase.update(TABLE_NAME, values, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }



    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int count;
        count = mDataBase.delete(TABLE_NAME, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    public Cursor getCursor_mode() {
        Cursor cursor_mode = null;
        try {
            SQLiteDatabase db = CoffeeExpressDBHelper.getInstance(getContext()).getReadableDatabase();
            cursor_mode = db.rawQuery("select * from coffee_express_table", null);

        } catch (Exception e) {
            Log.e("coffee", "findRecordRule Error" + e.getMessage());
            return null;
        }
        return cursor_mode;
    }

    public Cursor getCounts(String startdate, String enddate) {

        Log.d("Start Date:database", ":" + startdate);
        Log.d("End Date:database", ":" + enddate);
        try {
            Log.d("Veera", ":try");
            SQLiteDatabase db = CoffeeExpressDBHelper.getInstance(getContext()).getReadableDatabase();
           // db.rawQuery("UPDATE "+ TABLE_WORDS + " SET "+ KEY_WORD + " = '"+word.getWord()+"' ,"+KEY_DEFINITION+"= '"+word.getDefinition()+ "' WHERE " + KEY_ID + " = "+word.getID(), null);
            /*Cursor countDetails = db.rawQuery("select * from coffee_express_table " +
                    "  where  date >= Datetime('" + startdate + "') " +
                    "  and date <= Datetime('" + enddate + "')", null);*/

           /// Cursor countDetails = db.rawQuery("select * from coffee_express_table where date between '"+startdate+"' and '"+enddate+"'",null);

           Cursor countDetails = db.rawQuery("select * from coffee_express_table where date between '"+startdate+"' and '"+enddate+"'",null);



             // Cursor countDetails = db.rawQuery("Select * from  coffee_express_table  where date" + " > =" + startdate+ " AND date  < = " + enddate ,null);


            //;

            Log.d("mmm", "find Count Cursor " + countDetails.getCount());

/*
            if (countDetails != null && countDetails.moveToFirst()) {
                do {

                } while (countDetails.moveToNext());
                return countDetails;
            } else {
                return null;
            }*/
            return countDetails;
        } catch (Exception e) {
            Log.e("mmm", "find Count Cursor Error" + e);
            return null;
        }
    }
}

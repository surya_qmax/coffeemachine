package com.mmm.coffee_machine.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by surya on 26/12/18.
 */

public class serialNoTable extends ContentProvider {

    public static final String _ID = "_id";
    public static final String BOARD_SERIAL_NO = "serialno";

   // fields for my content provider
    public static final String PROVIDER_NAME = "com.mmm.coffee_machine.provider.status_serial";
    public static final String URL = "content://" + PROVIDER_NAME + "/coffee_express_table_serialno";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    public static final String TABLE_NAME = "coffee_express_table_serialno";

    private static String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "( " +
                    _ID + " integer primary key autoincrement , " +

                    BOARD_SERIAL_NO + " VARCHAR)";

    // mDataBase declarations
    private SQLiteDatabase mDataBase;

    public serialNoTable() {
    }

    public void createDB(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    public void upgradeDB(SQLiteDatabase sqLiteDatabase) {
        String GENERAL_DROP_TABLE = "DROP TABLE" + TABLE_NAME;
        sqLiteDatabase.execSQL(GENERAL_DROP_TABLE);
        createDB(sqLiteDatabase);
    }

    @Override
    public boolean onCreate() {
        // permissions to be writable
        mDataBase = CoffeeExpressDBHelper.getInstance(getContext()).getWritableDatabase();
        return mDataBase != null;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // the TABLE_NAME to query on
        queryBuilder.setTables(TABLE_NAME);
        Cursor cursor = queryBuilder.query(mDataBase, projection, selection,
                selectionArgs, null, null, null);
        /**
         * register to watch a content URI for changes
         */
        if (getContext() != null)
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long row = mDataBase.insert(TABLE_NAME, "", values);

        // If record is added successfully
        if (row > 0) {
            Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
            if (getContext() != null)
                getContext().getContentResolver().notifyChange(newUri, null);
            Log.v("mmm", "location added");
            return newUri;
        }
        throw new SQLException("Fail to add a new record into " + uri);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count;
        count = mDataBase.update(TABLE_NAME, values, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }



    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int count;
        count = mDataBase.delete(TABLE_NAME, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    public Cursor getserialno() {
        Cursor cursor_mode = null;
        try {
            SQLiteDatabase db = CoffeeExpressDBHelper.getInstance(getContext()).getReadableDatabase();
            cursor_mode = db.rawQuery("select * from serialNoTable", null);

        } catch (Exception e) {
            Log.e("coffee", "findRecordRule Error" + e.getMessage());
            return null;
        }
        return cursor_mode;
    }


}

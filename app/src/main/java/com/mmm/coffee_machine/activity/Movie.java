package com.mmm.coffee_machine.activity;

/**
 * Created by Lincoln on 15/01/16.
 */
public class Movie {
    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    private String count, genre;

    public Movie() {
    }

    public Movie( String genre, String count) {
        this.genre = genre;
        this.count = count;
    }


}

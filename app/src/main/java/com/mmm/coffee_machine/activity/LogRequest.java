package com.mmm.coffee_machine.activity;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mmm.coffee_machine.BLE.BluetoothLeService;
import com.mmm.coffee_machine.BLE.SampleGattAttributes;
import com.mmm.coffee_machine.CoffeeApplication;
import com.mmm.coffee_machine.R;
import com.mmm.coffee_machine.db.CoffeeExpressTable;
import com.mmm.coffee_machine.db.serialNoTable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

public class LogRequest extends AppCompatActivity {

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static final String BROADCAST_ACTION = "com.mmm.coffee_machine.activity.broadcastreceiverlogvalue";

    private final static String TAG = LogRequest.class.getSimpleName();
    public static TextView mReceiveByte;
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    Timer mytime = new Timer();

    TextView text_state;
    private TextView mDataField;
    private Button bLogRequest = null;
    private Button bOpenLog ;
    Button btnCountList=null;
    private Button bResetMemory;
    private String mDeviceName;
    private String mDeviceAddress;

    public  static  Context mcontxt;

    public static FileWriter fstream = null;
    public static BufferedWriter fbw;
    public static File input_file = null;
    private String logmonth;
    private int lineflag ;

    private int newline =0 ;
    private int newline1 = 0;
    public static int headerflag = 1;
    boolean mBlinking = false;

    MyBroadCastReceiver myBroadCastReceiver;



    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    // or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);

                text_state.setText("State : Connected");
                invalidateOptionsMenu();
                Log.v("mmmma", " DEVICE CONNECTED : " + BluetoothLeService.ACTION_GATT_CONNECTED.equals(action));

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);

                text_state.setText("State : DisConnected");
                invalidateOptionsMenu();
                Log.v("mmmma", " DEVICE DISCONNECTED : " + BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action));
                SharedPreferences sharedpreferences = getSharedPreferences(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, CoffeeApplication.STATUS_CONNECTED);
                editor_seq_no.commit();
                //clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                //displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v("mmmma", " RECEIVING BYTE : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

                String s = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);
                Log.v(TAG, "Receiving display data : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v(TAG, "Receiving display data : " + s);

            }
        }
    };
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
//                                mBluetoothLeService.setCharacteristicNotification(
//                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
//                            mBluetoothLeService.setCharacteristicNotification(
//                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
            };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.EXTRA_DATA);
        return intentFilter;
    }

    public static void setDataText(final long receive_byte_count) {
        LogRequest.mReceiveByte.setText("Receive data" + receive_byte_count);
    }



    private void registerMyReceiver() {

        try
        {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BROADCAST_ACTION);
            registerReceiver(myBroadCastReceiver, intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    /**
     * MyBroadCastReceiver is responsible to receive broadCast from register action
     * */
    class MyBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                Log.d(TAG, "onReceive() called");
                Animation anim = new AlphaAnimation(0.0f, 1.0f);


                if (intent != null) {
                    String dataReceived = intent.getStringExtra("data");

                    Log.d(TAG, "BluetoothLeService Display data received set logtext" + dataReceived);

                    if (dataReceived.equals("completed")) {

                        mDataField.clearAnimation();
                        mDataField.setAlpha(1.0f);
                        mBlinking = false;
                        mDataField.setText("Completed");

                    }

                    else if(dataReceived.equals("progress"))
                    {

                        anim.setDuration(10000);
                        anim.setStartOffset(100);
                        anim.setRepeatMode(Animation.REVERSE);
                        anim.setRepeatCount(Animation.RELATIVE_TO_PARENT);

                        if(!mBlinking){
                            mDataField.startAnimation(anim);
                            mBlinking = true;
                        } else{
                            mDataField.clearAnimation();
                            mDataField.setAlpha(1.0f);
                            mBlinking = false;
                        }

                        Log.d(TAG,"Display BluetoothLeService Display data received set logtext"+dataReceived);
                        mDataField.setText("File Transfer in progress...");

                    }

                }


            } catch (Exception e) {

            }
        }
    }



    private void clearUI() {
        //mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }

    public static void mCreateReceiveFile() {
        Log.v(TAG, "mCreateReceiveFile input file created");

        String fileName = null;
        /**
         * Date and time for filename creation
         */
        input_file = null;
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat monthdate=new SimpleDateFormat("MMM");

        int filemonth = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH,filemonth);
        String monthname=monthdate.format(calendar.getTime());

        int year = calendar.get(Calendar.YEAR);


        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);        // 24 hour clock
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        String extr = Environment.getExternalStorageDirectory().toString();
        Log.d(TAG, "Folder path " + extr);
        File mFolder = new File(extr + "/" + "CoffeeMachine");
        Log.d(TAG, "DEVICE COMMUNICATION Folder path1 " + mFolder.getAbsolutePath());
        if (!mFolder.exists()) {
            mFolder.mkdir();
            Log.d(TAG, "DEVICE COMMUNICATION New Folder Created");
        } else
            Log.d(TAG, "DEVICE COMMUNICATION Folder already exits");

//        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
//        String month_name = month_date.format(month+1);




        fileName =
                dayOfMonth + "_" + monthname + "_" + year + "_" + hourOfDay + "" + minute + "" + second + ".txt";
        //FILENAME = new File(filefolder + "/" + fileName);

        input_file = new File(mFolder.getAbsolutePath() + "/" + fileName);

        try {
            input_file.createNewFile();

            Log.v(TAG, "mCreateReceiveFile input file created" + input_file.getAbsolutePath());
//          mReceiveFOS = new FileOutputStream(receiveFile, true);

            // Write header information details are : Switch status, Mode Settings, Error code
            String textToHeaderString = "";
            //Get enable swtich status
            //Display Current date




            String todayDate =   dayOfMonth +  monthname +  year + " " + hourOfDay + ":" + minute + ":" + second ;

            // mode settings

            // mode settings
            Log.v("MMMEEEE", "KKKK DEVICE COMMUNICATION Mode selection selectedModeName1 : "+ todayDate);
/*
            ModeTimeTable nodeTableObj = new ModeTimeTable();

            Cursor mSelectedModeCursor = nodeTableObj.getCursor_mode();*/

            Log.v("MMMEEEE", "KKKK DEVICE COMMUNICATION Mode selection selectedModeName2 : ");





            headerflag = 1;


            if(headerflag ==1) {

                String serialno ="";

                Cursor cursorserial = mcontxt.getContentResolver().query(serialNoTable.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);
                Log.d("coffee log", "print11");

                if (cursorserial != null && cursorserial.moveToFirst()) {
                    do {
                        Log.d("coffee log", "do");


                         serialno = cursorserial.getString(cursorserial.getColumnIndex(serialNoTable.BOARD_SERIAL_NO));
                    } while(cursorserial.moveToNext());
                    cursorserial.close();
                }


                        textToHeaderString = "\t\t" + "Fair Deal Coffee Express " + "\n____________________________________________\n" + "\n" + "Board Name : Coffee Express" + "\n___________________________________________\n" + "Board Serial No  : " + serialno + "\n" + "Date : " + todayDate + "\n____________________________________________\n";



                Log.v("MMMEEEE", "DEVICE COMMUNICATION   : " + textToHeaderString);
                textToHeaderString = textToHeaderString + "\n\n" + "__________________________________________________\n";
                textToHeaderString = textToHeaderString + "\n\n       Variants       Variants Description\n" +
                        "__________________________________________________\n" +
                        "        BT              Black Tea\n" +
                        "        BC              Black Coffee\n" +
                        "        LT              Light Tea\n" +
                        "        LC              Light Coffee\n" +
                        "        ST              Strong Tea\n" +
                        "        SC              Strong Coffee\n" +
                        "        HM              Hot Milk\n" +

                        "_______________________________________________________________________      \n" +
                        "   DATE\t      BT\t  BC\t  LT\t  LC\t  ST\t  SC\t  HM\n" +
                        "_______________________________________________________________________";



                Cursor cursor = mcontxt.getContentResolver().query(CoffeeExpressTable.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);
                Log.d("coffee log", "print11");

                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        Log.d("coffee log", "do");

                        String strDate = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.DATE));


                        Date date = null;
                        try {
                            date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String formattedDate = new SimpleDateFormat("dd MMM yyyy").format(date);




                        String strBTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BTCMSB));
                        String strBTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BTCLSB));

                        // String blacktea = strBTCMSB + strBTCLSB;


                        int BTCCount = (Integer.parseInt(strBTCMSB)<<8)+Integer.parseInt(strBTCLSB);

                        String BTCCountlen = String.valueOf(BTCCount);

                        String BTCountSpace="";

                        if(BTCCountlen.length()==4)
                        {
                            BTCountSpace = "";
                        }
                        else if(BTCCountlen.length()==3)
                        {
                            BTCountSpace = " ";
                        }
                        else if(BTCCountlen.length()==2)
                        {
                            BTCountSpace = "  ";

                        }
                        else if(BTCCountlen.length()==1)
                        {
                            BTCountSpace = "   ";

                        }



                        Log.d("blacktea integer value", "blacktea integer value::" + BTCCount);

                        //TXT_BLACK_TEA_COUNT.setText(strBTCMSB + strBTCLSB);
                       // TXT_BLACK_TEA_COUNT.setText(String.valueOf(BTCCount));

                        String strLTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LTCMSB));
                        String strLTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LTCLSB));
                        Log.d("Test", "::" + strLTCMSB);
                        Log.d("Test", "::" + strLTCLSB);
                        // String lighttea = strLTCMSB + strLTCLSB;
                        int LTCount = (Integer.parseInt(strLTCMSB)<<8)+Integer.parseInt(strLTCLSB);

                        String LTCountlen = String.valueOf(LTCount);
                        String LTCountSpace="";


                        if(LTCountlen.length()==4)
                        {
                            LTCountSpace = "";
                        }
                        else if(LTCountlen.length()==3)
                        {
                            LTCountSpace = " ";
                        }
                        else if(LTCountlen.length()==2)
                        {
                            LTCountSpace = "  ";

                        }
                        else if(LTCountlen.length()==1)
                        {
                            LTCountSpace = "   ";

                        }



                        Log.d("blacktea", "::" + LTCount);
                        //TXT_LIGHT_TEA_COUNT.setText(strLTCMSB + strLTCLSB);
                       // TXT_LIGHT_TEA_COUNT.setText(String.valueOf(LTCount));

                        String strSCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.SCCMSB));
                        String strSCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.SCCLSB));
                        //  String stringcoffee = strSCCMSB + strSCCLSB;

                        int SCCount = (Integer.parseInt(strSCCMSB)<<8)+Integer.parseInt(strSCCLSB);

                        String SCCountlen = String.valueOf(SCCount);

                        String SCCountSpace="";


                        if(SCCountlen.length()==4)
                        {
                            SCCountSpace = "";
                        }
                        else if(SCCountlen.length()==3)
                        {
                            SCCountSpace = " ";
                        }
                        else if(SCCountlen.length()==2)
                        {
                            SCCountSpace = "  ";

                        }
                        else if(SCCountlen.length()==1)
                        {
                            SCCountSpace = "   ";

                        }

                        Log.d("blacktea", "::" + SCCount);
                        //TXT_STRONG_COFFEE_COUNT.setText(strSCCMSB + strSCCLSB);
                       // TXT_STRONG_COFFEE_COUNT.setText(String.valueOf(SCCount));

                        String strLCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LCCMSB));
                        String strLCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LCCLSB));
                        // String lightcoffee = strLCCMSB + strLCCLSB;
                        //Log.d("blacktea", "::" + lightcoffee);

                        int LCCount = (Integer.parseInt(strLCCMSB)<<8)+Integer.parseInt(strLCCLSB);

                        String LCCounttlen = String.valueOf(LCCount);

                        String LCCountSpace="";


                        if(LCCounttlen.length()==4)
                        {
                            LCCountSpace = "";
                        }
                        else if(LCCounttlen.length()==3)
                        {
                            LCCountSpace = " ";
                        }
                        else if(LCCounttlen.length()==2)
                        {
                            LCCountSpace = "  ";

                        }
                        else if(LCCounttlen.length()==1)
                        {
                            LCCountSpace = "   ";

                        }


                        //TXT_LIGHT_COFFEE_COUNT.setText(strLCCMSB + strLCCLSB);
                       // TXT_LIGHT_COFFEE_COUNT.setText(String.valueOf(LCCount));

                        String strBCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BCCMSB));
                        String strBCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BCCLSB));
                        // String blackcoffee = strBCCMSB + strBCCLSB;
                        // Log.d("blacktea", "::" + blackcoffee);
                        int BCCount = (Integer.parseInt(strBCCMSB)<<8)+Integer.parseInt(strBCCLSB);

                        String BCCounttlen = String.valueOf(BCCount);

                        String BCCountSpace="";


                        if(BCCounttlen.length()==4)
                        {
                            BCCountSpace = "";
                        }
                        else if(BCCounttlen.length()==3)
                        {
                            BCCountSpace = " ";
                        }
                        else if(BCCounttlen.length()==2)
                        {
                            BCCountSpace = "  ";

                        }
                        else if(BCCounttlen.length()==1)
                        {
                            BCCountSpace = "   ";

                        }


                        // TXT_BLACK_COFFEE_COUNT.setText(strBCCMSB + strBCCLSB);
                      //  TXT_BLACK_COFFEE_COUNT.setText(String.valueOf(BCCount));

                        String strSTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.STCMSB));
                        String strSTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.STCLSB));
                        //String strongtea = strSTCMSB + strSTCLSB;
                        //Log.d("blacktea", "::" + strongtea);
                        // TXT_STRONG_TEA_COUNT.setText(strSTCMSB + strSTCLSB);

                        int STCount = (Integer.parseInt(strSTCMSB)<<8)+Integer.parseInt(strSTCLSB);


                        String STCounttlen = String.valueOf(STCount);

                        String STCountSpace="";


                         if(STCounttlen.length()==4)
                        {
                            STCountSpace = "";
                        }
                        else if(LCCounttlen.length()==3)
                        {
                            STCountSpace = " ";
                        }
                        else if(STCounttlen.length()==2)
                        {
                            STCountSpace = "  ";

                        }
                        else if(STCounttlen.length()==1)
                        {
                            STCountSpace = "   ";

                        }

                      //  TXT_STRONG_TEA_COUNT.setText(String.valueOf(STCount));


                        String strHMCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.HMCMSB));
                        String strHMCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.HMCLSB));
                        String hotmilk = strHMCMSB + strHMCLSB;
                        Log.d("blacktea", "::" + hotmilk);

                        int HMCount = (Integer.parseInt(strHMCMSB)<<8)+Integer.parseInt(strHMCLSB);

                        String HMCountSpace="";


                        String HMCounttlen = String.valueOf(HMCount);

                         if(HMCounttlen.length()==4)
                        {
                            HMCountSpace = "";
                        }
                        else if(HMCounttlen.length()==3)
                        {
                            HMCountSpace = " ";
                        }
                        else if(HMCounttlen.length()==2)
                        {
                            HMCountSpace = "  ";

                        }
                        else if(HMCounttlen.length()==1)
                        {
                            HMCountSpace = "   ";

                        }




                        // TXT_HOT_MILK_COUNT.setText(strHMCMSB + strHMCLSB);
                       // TXT_HOT_MILK_COUNT.setText(String.valueOf(HMCount));






                      /*  String strBTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BTCMSB));
                        String strBTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BTCLSB));
                        String blacktea = strBTCMSB + strBTCLSB;


                        // TXT_BLACK_TEA_COUNT.setText(blacktea);

                        String strLTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LTCMSB));
                        String strLTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LTCLSB));
                        Log.d("Test", "::" + strLTCMSB);
                        Log.d("Test", "::" + strLTCLSB);
                        String lighttea = strLTCMSB + strLTCLSB;
                        Log.d("blacktea log", "::" + lighttea);

                        // TXT_LIGHT_TEA_COUNT.setText(lighttea);

                        String strSCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.SCCMSB));
                        String strSCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.SCCLSB));
                        String stringcoffee = strSCCMSB + strSCCLSB;
                        Log.d("blacktea log", "::" + stringcoffee);

                        // TXT_STRONG_COFFEE_COUNT.setText(stringcoffee);

                        String strLCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LCCMSB));
                        String strLCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LCCLSB));
                        String lightcoffee = strLCCMSB + strLCCLSB;
                        Log.d("blacktea log", "::" + lightcoffee);

                        //TXT_LIGHT_COFFEE_COUNT.setText(lightcoffee);

                        String strBCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BCCMSB));
                        String strBCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BCCLSB));
                        String blackcoffee = strBCCMSB + strBCCLSB;
                        Log.d("blacktea log", "::" + blackcoffee);

                        // TXT_BLACK_COFFEE_COUNT.setText(blackcoffee);

                        String strSTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.STCMSB));
                        String strSTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.STCLSB));
                        String strongtea = strSTCMSB + strSTCLSB;
                        Log.d("blacktea log", "::" + strongtea);

                        //TXT_STRONG_TEA_COUNT.setText(strongtea);


                        String strHMCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.HMCMSB));
                        String strHMCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.HMCLSB));
                        String hotmilk = strHMCMSB + strHMCLSB;
                        Log.d("blacktea log", "::" + hotmilk);

                        //TXT_HOT_MILK_COUNT.setText(hotmilk);*/

                        textToHeaderString = textToHeaderString+"\n"+formattedDate+ "\t"+BTCountSpace+BTCCount + "\t"+BCCountSpace+ BCCount  +"\t"+LTCountSpace+ LTCount  + "\t"+LCCountSpace+ LCCount  +  "\t"+STCountSpace+ STCount + "\t"+SCCountSpace+SCCount +"\t"+HMCountSpace+HMCount ;




                    }
                    while (cursor.moveToNext());
                    cursor.close();
                }



                Log.v("MMMEEEE", "DEVICE COMMUNICATION Mode selection selectedModeName7 : " + textToHeaderString);

                fstream = new FileWriter(input_file.getAbsolutePath(), true);
                fbw = new BufferedWriter(fstream);
                fbw.write(String.valueOf(textToHeaderString));
                Log.d(TAG, "HEADER TEXT ERROR" + String.valueOf(textToHeaderString));
                fbw.newLine();
                fbw.close();
                Log.v(TAG, "DEVICE COMMUNICATION input file created");

                headerflag = 0;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_request);

       // btntextlog

        mDataField =  (TextView) findViewById(R.id.btntextlog);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        text_state = (TextView) toolbar.findViewById(R.id.txt_state_log);
        setSupportActionBar(toolbar);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        mcontxt = getApplicationContext();


        Button btnCountLog = (Button) findViewById(R.id.btn_count_log);


        btnCountLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent intentCountLog = new Intent(getApplicationContext(), CountLog.class);
                 startActivity(intentCountLog);
            }
        });

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        bLogRequest = (Button) findViewById(R.id.btn_log_request);

        bOpenLog = (Button) findViewById(R.id.btn_open_log);

        bOpenLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG,"Create File called");



                openFolder("CoffeeMachine");


            }
        });


        bLogRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send byte to device for request log file length

                if (mBluetoothLeService != null) {
                    BluetoothLeService.RECEIVE_BYTE_COUNT = 0;
                    try {
                        byte[] BYTE_SEND_LOG_REQUEST = {
                                0x41, 0x54, 0x02, 0x07, 0x03, 0x04, 0x05,
                                0x06, 0x07, 0x08, 0x09, 0x41,
                                0x56, 0x45, 0x66, 0x6d, 0x74,
                                0x20, 0x0D, 0x0A};
                        mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                        Log.v(TAG, "BYTE*** DEVICE COMMUNICATION Write 27...");
                        Toast.makeText(getApplicationContext(), "Send Request Log Request command Successfully (Write function 21...)", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        btnCountList= (Button) findViewById(R.id.btn_count_details);
        btnCountList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCountLog = new Intent(getApplicationContext(), CountList.class);
                startActivity(intentCountLog);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case R.id.menu_connect:
                    mBluetoothLeService.connect(mDeviceAddress);
                    text_state.setText("State : Connected");
                    return true;
                case R.id.menu_disconnect:
                    mBluetoothLeService.disconnect();
                    text_state.setText("State : DisConnected");
                    return true;
                case android.R.id.home:
                    onBackPressed();
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Log Connect request result=" + result);
        }

        myBroadCastReceiver = new MyBroadCastReceiver();
        registerMyReceiver();


    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedpreferences = getSharedPreferences(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
        editor_seq_no.clear();
        editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, CoffeeApplication.STATUS_DISCONNECTED);
        editor_seq_no.commit();
//        try {
//            unbindService(mServiceConnection);
//            mBluetoothLeService.disconnect();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mBluetoothLeService = null;
        try {
            mytime.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //mConnectionState.setText(resourceId);
            }
        });
    }

    public void openFolder(String mFoldername) {

        Uri path= Uri.parse(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+mFoldername+"/");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(path, "resource/folder");     // **//or "text/csv" for Other Types**
        startActivity(Intent.createChooser(intent, "Open folder"));
      }

    private void displayData(String data) {
        if (data != null) {
            mDataField.setText(data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2},
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2}
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


}

package com.mmm.coffee_machine.activity;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mmm.coffee_machine.R;
import com.mmm.coffee_machine.db.CoffeeExpressTable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CountList extends AppCompatActivity {

    private ListView listview_count;
    private String startdate, enddate;
    private TextView txt_startDate, txt_EndDate;

    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;

    String startdateAsString;


    ArrayList Blacktea = new ArrayList();
    ArrayList Blackcoffee = new ArrayList();
    ArrayList Strongtea = new ArrayList();
    ArrayList Lighttea = new ArrayList();
    ArrayList Lightcoffee = new ArrayList();
    ArrayList Strongcoffee = new ArrayList();
    ArrayList Hotmilk = new ArrayList();
    private List<Movie> movieList = new ArrayList<>();

    Calendar myCalendar = Calendar.getInstance();
    Calendar myCalendar_ = Calendar.getInstance();

    int blackteacount =0, LightTeacount=0, StrongTeaCount=0;
    int blacktcoffeecount =0, LightCoffeecount=0, StrongCoffeeCount=0, HotMilkCount =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_list);

        ImageView imgpickdate= (ImageView) findViewById(R.id.pickdatelist);

        imgpickdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }
                };

                DatePickerDialog mDatePickerDialog = new DatePickerDialog(CountList.this,date, myCalendar_
                        .get(Calendar.YEAR), myCalendar_.get(Calendar.MONTH),
                        myCalendar_.get(Calendar.DAY_OF_MONTH));

                mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePickerDialog.show();


            }
        });

        //listview_count = (ListView) findViewById(R.id.count_listview);
        txt_startDate = (TextView) findViewById(R.id.txt_start_date);
        txt_EndDate = (TextView) findViewById(R.id.txt_end_date);
//count_Log_date1
        long date = System.currentTimeMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = sdf.format(date);
        TextView count_Log_date1 = (TextView) findViewById(R.id.count_Log_date1);
        count_Log_date1.setText(dateString);

        recyclerView = (RecyclerView) findViewById(R.id.count_listview);





      //  prepareMovieData();


    }
    private void prepareMovieData() {


        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_count_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        DatePickerDialog mDatePickerDialog = new DatePickerDialog(CountList.this,date, myCalendar_
                .get(Calendar.YEAR), myCalendar_.get(Calendar.MONTH),
                myCalendar_.get(Calendar.DAY_OF_MONTH));

        mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePickerDialog.show();




        mAdapter = new MoviesAdapter(Blacktea,Lighttea,Strongtea,Blackcoffee ,Lightcoffee,Strongcoffee,Hotmilk);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        Blacktea.add(blackteacount);
        Lighttea.add(LightTeacount);
        Strongtea.add(StrongTeaCount);
        Blackcoffee.add(blacktcoffeecount);
        Lightcoffee.add(LightCoffeecount);
        Strongcoffee.add(StrongCoffeeCount);
        Hotmilk.add(HotMilkCount);

        mAdapter.notifyDataSetChanged();


       // mAdapter.notifyDataSetChanged();


        // row click listener
        /*recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Movie movie = movieList.get(position);
                Toast.makeText(getApplicationContext(), movie.getGenre() + movie.getCount() +" is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
*/



        /*new DatePickerDialog(CountList.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();*/
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yy"; //In which you need put here
        String myDate = "dd";
        String myMonthFormat = "MM";
        String myYearFormat = "yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdfD = new SimpleDateFormat(myDate, Locale.US);

        SimpleDateFormat sdfM = new SimpleDateFormat(myMonthFormat, Locale.US);

        SimpleDateFormat sdfY = new SimpleDateFormat(myYearFormat, Locale.US);

        txt_startDate.setText("Start Date: " + sdf.format(myCalendar.getTime()));
        //startdate = sdf.format(myCalendar.getTime());


        String date = sdfD.format(myCalendar.getTime());
        String month = sdfM.format(myCalendar.getTime());
        String year = sdfY.format(myCalendar.getTime());

        startdate = year + "-" + month + "-" + date;
        Log.d("MMMM", "Start Date" + ":" + startdate);
        //Get Details from table


        try {
            DatePickerDialog.OnDateSetListener date_ = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar_.set(Calendar.YEAR, year);
                    myCalendar_.set(Calendar.MONTH, monthOfYear);
                    myCalendar_.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel_();
                }
            };
           /* new DatePickerDialog(CountList.this, date_, myCalendar_
                    .get(Calendar.YEAR), myCalendar_.get(Calendar.MONTH),
                    myCalendar_.get(Calendar.DAY_OF_MONTH)).show();
*/

           /* DatePickerDialog mDatePickerDialog = new DatePickerDialog(CountList.this,date_, myCalendar_
                    .get(Calendar.YEAR), myCalendar_.get(Calendar.MONTH),
                    myCalendar_.get(Calendar.DAY_OF_MONTH));

            mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            mDatePickerDialog.show();*/
            long startdatetimeInMilliseconds=0;
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date mDate = sdf1.parse(startdate);
                startdatetimeInMilliseconds = mDate.getTime();
                System.out.println("Date in milli :: " + startdatetimeInMilliseconds);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            DatePickerDialog mDatePickerDialog = new DatePickerDialog(CountList.this,date_, myCalendar_
                    .get(Calendar.YEAR), myCalendar_.get(Calendar.MONTH),
                    myCalendar_.get(Calendar.DAY_OF_MONTH));

            mDatePickerDialog.getDatePicker().setMinDate(startdatetimeInMilliseconds);
            mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

            mDatePickerDialog.show();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void updateLabel_() {

        String myFormat = "dd/MM/yy"; //In which you need put here
        String myDate = "dd";
        String myMonthFormat = "MM";
        String myYearFormat = "yyyy";





        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdfD = new SimpleDateFormat(myDate, Locale.US);

        SimpleDateFormat sdfM = new SimpleDateFormat(myMonthFormat, Locale.US);

        SimpleDateFormat sdfY = new SimpleDateFormat(myYearFormat, Locale.US);

        txt_EndDate.setText("End Date: " + sdf.format(myCalendar_.getTime()));
        //enddate = sdf.format(myCalendar_.getTime());

        String date = sdfD.format(myCalendar_.getTime());
        String month = sdfM.format(myCalendar_.getTime());
        String year = sdfY.format(myCalendar_.getTime());

        enddate = year + "-" + month + "-" + date;
        Log.d("End Date", ":" + enddate);


        Blackcoffee.clear();
        Blacktea.clear();
        Strongcoffee.clear();
        Hotmilk.clear();
        Strongtea.clear();
        Lightcoffee.clear();
        Lighttea.clear();



        CoffeeExpressTable ObjCoffeeTable = new CoffeeExpressTable();

        Cursor todoCursor = ObjCoffeeTable.getCounts(startdate, enddate);


        if (todoCursor != null) {
            Log.d("GetData", " :if"+todoCursor.getCount());
        } else {
            Log.d("GetData", " :else");
        }
        if (todoCursor != null && todoCursor.getCount()!=0) {

            todoCursor.moveToFirst();


            String result, first, second;
            String resultLightTea, firstLightTea, secondLightTea;
            String resultStrongTea, firstStrongTea, secondStrongTea;
            String resultBlackCoffee, firstBlackCoffee, secondBlackcoffee;
            String resultLightCoffee, firstLightCoffee, secondLightCoffee;
            String resultStrongCoffee, firstStrongCoffee, secondStrongCoffee;
            String resultHotMilk, firstHotMilk, secondHotMilk;
            int BTCount, BCCount, LTCount, LCCount, STCount, SCCount, HMCount;



            do {

             //   String datereceived = (todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.DATE)));


                first = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCMSB));
                second = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCLSB));
                result = first + second;
                BTCount = (Integer.parseInt(first)<<8)+Integer.parseInt(second);

                blackteacount += BTCount;
                Log.d("blacktea cumulative", "::" + blackteacount);
                //Log.d("Blacktead", ":" + result);

            //    Blacktea.add(blackteacount);



                firstLightTea = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.LTCMSB));
                secondLightTea = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.LTCLSB));
                resultLightTea = firstLightTea + secondLightTea;
                LTCount = (Integer.parseInt(firstLightTea)<<8)+Integer.parseInt(secondLightTea);

                LightTeacount += LTCount;
                Log.d("LightTea cumulative", "::" + LightTeacount);


               // Lighttea.add(LightTeacount);


                firstStrongTea = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.STCMSB));
                secondStrongTea = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.STCLSB));
               // resultStrongTea = firstStrongTea + secondStrongTea;

                STCount = (Integer.parseInt(firstStrongTea)<<8)+Integer.parseInt(secondStrongTea);

                StrongTeaCount += STCount;


                Log.d("StrongTea cumulative", "::" + StrongTeaCount);



                firstBlackCoffee = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BCCMSB));
                secondBlackcoffee = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BCCLSB));
                //resultBlackCoffee = firstBlackCoffee + secondBlackcoffee;
                //blacktcoffeecount += Integer.parseInt(resultBlackCoffee);


                BCCount = (Integer.parseInt(firstBlackCoffee)<<8)+Integer.parseInt(secondBlackcoffee);

                blacktcoffeecount += BCCount;


                Log.d("StrongTea cumulative", "::" + StrongTeaCount);


                Log.d("blackcoffee cumulative", "::" + blacktcoffeecount);
                //Log.d("Blackcoffee", ":" + resultBlackCoffee);

               // Blackcoffee.add(blacktcoffeecount);


                firstLightCoffee = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.LCCMSB));
                secondLightCoffee = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.LCCLSB));
               // resultLightCoffee = firstLightCoffee + secondLightCoffee;

                LCCount = (Integer.parseInt(firstLightCoffee)<<8)+Integer.parseInt(secondLightCoffee);

                LightCoffeecount += LCCount;
                Log.d("LightCoffee cumulative", "::" + LightCoffeecount);

               // Lightcoffee.add(LightCoffeecount);



                firstStrongCoffee = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.SCCMSB));
                secondStrongCoffee = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.SCCLSB));
              //  resultStrongCoffee = firstStrongCoffee + secondStrongCoffee;
               // StrongCoffeeCount += Integer.parseInt(resultStrongCoffee);

                SCCount = (Integer.parseInt(firstStrongCoffee)<<8)+Integer.parseInt(secondStrongCoffee);

                StrongCoffeeCount += SCCount;

                Log.d("StrongCoffee cumulative", "::" + StrongCoffeeCount);


                // Strongcoffee.add(StrongCoffeeCount);




                firstHotMilk = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.HMCMSB));
                secondHotMilk = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.HMCLSB));
                //resultHotMilk = firstHotMilk + secondHotMilk;

                HMCount = (Integer.parseInt(firstHotMilk)<<8)+Integer.parseInt(secondHotMilk);



                HotMilkCount += HMCount;
                Log.d("StrongCoffee cumulative", "::" + HotMilkCount);

              /*  first = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCMSB));
                second = todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCLSB));
                result = first + second;
                blackteacount += Integer.parseInt(result);
                Log.d("blacktea cumulative", "::" + blackteacount);
                Log.d("Blacktead", ":" + result);
                */



           //Blackcoffee.add(result);
//           Lighttea.add(todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCLSB)));
//           Lightcoffee.add(todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCLSB)));
//           Strongtea.add(todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCLSB)));
//           Strongcoffee.add(todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCLSB)));
//           Hotmilk.add(todoCursor.getString(todoCursor.getColumnIndex(CoffeeExpressTable.BTCLSB)));
            } while (todoCursor.moveToNext());



            Blacktea.add(blackteacount);
            Lighttea.add(LightTeacount);
            Strongtea.add(StrongTeaCount);
            Blackcoffee.add(blacktcoffeecount);
            Lightcoffee.add(LightCoffeecount);
            Strongcoffee.add(StrongCoffeeCount);
            Hotmilk.add(HotMilkCount);

            mAdapter.notifyDataSetChanged();


            // Setup cursor adapter using cursor from last step
           /* TodoCursorAdapter todoAdapter = new TodoCursorAdapter(this, todoCursor);
            // Attach cursor adapter to the ListView
            listview_count.setAdapter(todoAdapter);*/

            /*Movie movie ;

            movie= new Movie("Black Tea",String.valueOf(blackteacount));
            movieList.clear();
            //movieList.add(movie);

            movie= new Movie("Light Tea",String.valueOf(LightTeacount));
            movieList.add(movie);

            movie= new Movie("Strong Tea",String.valueOf(StrongTeaCount));
            movieList.add(movie);

            movie= new Movie("Black coffee",String.valueOf(blacktcoffeecount));
            movieList.add(movie);

            movie= new Movie("Light Coffee",String.valueOf(LightCoffeecount));
            movieList.add(movie);

            movie= new Movie("Strong Coffee",String.valueOf(StrongCoffeeCount));
            movieList.add(movie);*/




            // prepareMovieData();

        }

        else
        {
            Log.d("Blacktead", ":" + "else getcount");

            Blacktea.add(blackteacount);
            Lighttea.add(LightTeacount);
            Strongtea.add(StrongTeaCount);
            Blackcoffee.add(blacktcoffeecount);
            Lightcoffee.add(LightCoffeecount);
            Strongcoffee.add(StrongCoffeeCount);
            Hotmilk.add(HotMilkCount);

            mAdapter.notifyDataSetChanged();

        }
    }
}

package com.mmm.coffee_machine.activity;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.mmm.coffee_machine.R;
import com.mmm.coffee_machine.db.CoffeeExpressTable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CountLog extends AppCompatActivity {
    TextView txt_date;
    TextView TXT_BLACK_TEA_COUNT;
    TextView TXT_LIGHT_TEA_COUNT;
    TextView TXT_STRONG_TEA_COUNT;
    TextView TXT_BLACK_COFFEE_COUNT;
    TextView TXT_LIGHT_COFFEE_COUNT;
    TextView TXT_STRONG_COFFEE_COUNT;
    TextView TXT_HOT_MILK_COUNT;

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_log);

        ImageView imgPickDate = (ImageView) findViewById(R.id.pickdate);
        imgPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                TXT_BLACK_TEA_COUNT.setText("00");
                TXT_LIGHT_TEA_COUNT.setText("00");
                TXT_STRONG_TEA_COUNT.setText("00");
                TXT_BLACK_COFFEE_COUNT.setText("00");
                TXT_LIGHT_COFFEE_COUNT.setText("00");
                TXT_STRONG_COFFEE_COUNT.setText("00");
                TXT_HOT_MILK_COUNT.setText("00");


                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        view.setMaxDate(System.currentTimeMillis());
                        updateLabel();
                    }
                };

                DatePickerDialog mDatePickerDialog = new DatePickerDialog(CountLog.this,date,myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePickerDialog.show();
            }
        });

        txt_date = (TextView) findViewById(R.id.count_Log_date);

        TXT_BLACK_TEA_COUNT = (TextView) findViewById(R.id.black_tea_count);
        TXT_LIGHT_TEA_COUNT = (TextView) findViewById(R.id.light_tea_count);
        TXT_STRONG_TEA_COUNT = (TextView) findViewById(R.id.strong_tea_count);
        TXT_BLACK_COFFEE_COUNT = (TextView) findViewById(R.id.black_coffee_count);
        TXT_LIGHT_COFFEE_COUNT = (TextView) findViewById(R.id.light_coffee_count);
        TXT_STRONG_COFFEE_COUNT = (TextView) findViewById(R.id.strong_coffee_count);
        TXT_HOT_MILK_COUNT = (TextView) findViewById(R.id.hot_milk_count);

        TXT_BLACK_TEA_COUNT.setText("00");
        TXT_LIGHT_TEA_COUNT.setText("00");
        TXT_STRONG_TEA_COUNT.setText("00");
        TXT_BLACK_COFFEE_COUNT.setText("00");
        TXT_LIGHT_COFFEE_COUNT.setText("00");
        TXT_STRONG_COFFEE_COUNT.setText("00");
        TXT_HOT_MILK_COUNT.setText("00");
    }


    private void updateLabel() {

        String myFormat = "dd/MM/yy"; //In which you need put here
        String myDate = "dd";
        String myMonthFormat = "MM";
        String myYearFormat = "yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdfD = new SimpleDateFormat(myDate, Locale.US);

        SimpleDateFormat sdfM = new SimpleDateFormat(myMonthFormat, Locale.US);

        SimpleDateFormat sdfY = new SimpleDateFormat(myYearFormat, Locale.US);


        txt_date.setText(sdf.format(myCalendar.getTime()));

        String date = sdfD.format(myCalendar.getTime());
        //String date="3";
        String month = sdfM.format(myCalendar.getTime());
        String year = sdfY.format(myCalendar.getTime());


        Log.d("Current Selected date", "::" + date);
        Log.d("Current Selected month", "::" + month);
        Log.d("Current Selected year", "::" + year);

        String dt=year+"-"+month+"-"+date;

        Log.d("MMM","Selected date"+dt);

        ///////////Get Details from table
        try {
            Log.d("coffee", "print1");
//            Cursor cursor = getApplicationContext().getContentResolver().query(CoffeeExpressTable.CONTENT_URI,
//                    null,
//                    CoffeeExpressTable.DATE + "=? AND " + CoffeeExpressTable.MONTH + "=? AND " + CoffeeExpressTable.YEAR + "=? ",
//                    new String[]{date, month, year},
//                    null);
            Cursor cursor = getApplicationContext().getContentResolver().query(CoffeeExpressTable.CONTENT_URI,
                    null,
                    CoffeeExpressTable.DATE + "=? ",
                    new String[]{dt},
                    null);
            Log.d("coffee", "print11");



           /* CoffeeExpressTable coffeeTableObj = new CoffeeExpressTable();
            Cursor mCursor = coffeeTableObj.getCursor_mode();

            if (mCursor != null && mCursor.moveToFirst()) {
                Log.d("DATA",":"+"datafind");
            }
            else
            {
                Log.d("DATA",":"+"Nodatafind");
            }*/
            //TXT_BLACK_TEA_COUNT.setText("CCCBBBB"+date+" : "+month+ " : "+year);


            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Log.d("coffee", "do");

                    String strBTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BTCMSB));
                    String strBTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BTCLSB));

                   // String blacktea = strBTCMSB + strBTCLSB;


                    int BTCCount = (Integer.parseInt(strBTCMSB)<<8)+Integer.parseInt(strBTCLSB);
                    Log.d("blacktea integer value", "blacktea integer value::" + BTCCount);

                    //TXT_BLACK_TEA_COUNT.setText(strBTCMSB + strBTCLSB);
                    TXT_BLACK_TEA_COUNT.setText(String.valueOf(BTCCount));

                    String strLTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LTCMSB));
                    String strLTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LTCLSB));
                    Log.d("Test", "::" + strLTCMSB);
                    Log.d("Test", "::" + strLTCLSB);
                   // String lighttea = strLTCMSB + strLTCLSB;
                    int LTCount = (Integer.parseInt(strLTCMSB)<<8)+Integer.parseInt(strLTCLSB);



                    Log.d("blacktea", "::" + LTCount);
                    //TXT_LIGHT_TEA_COUNT.setText(strLTCMSB + strLTCLSB);
                    TXT_LIGHT_TEA_COUNT.setText(String.valueOf(LTCount));

                    String strSCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.SCCMSB));
                    String strSCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.SCCLSB));
                  //  String stringcoffee = strSCCMSB + strSCCLSB;

                    int SCCount = (Integer.parseInt(strSCCMSB)<<8)+Integer.parseInt(strSCCLSB);


                    Log.d("blacktea", "::" + SCCount);
                    //TXT_STRONG_COFFEE_COUNT.setText(strSCCMSB + strSCCLSB);
                    TXT_STRONG_COFFEE_COUNT.setText(String.valueOf(SCCount));

                    String strLCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LCCMSB));
                    String strLCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.LCCLSB));
                   // String lightcoffee = strLCCMSB + strLCCLSB;
                    //Log.d("blacktea", "::" + lightcoffee);

                    int LCCount = (Integer.parseInt(strLCCMSB)<<8)+Integer.parseInt(strLCCLSB);

                    //TXT_LIGHT_COFFEE_COUNT.setText(strLCCMSB + strLCCLSB);
                    TXT_LIGHT_COFFEE_COUNT.setText(String.valueOf(LCCount));

                    String strBCCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BCCMSB));
                    String strBCCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.BCCLSB));
                   // String blackcoffee = strBCCMSB + strBCCLSB;
                   // Log.d("blacktea", "::" + blackcoffee);
                    int BCCount = (Integer.parseInt(strBCCMSB)<<8)+Integer.parseInt(strBCCLSB);

                    // TXT_BLACK_COFFEE_COUNT.setText(strBCCMSB + strBCCLSB);
                    TXT_BLACK_COFFEE_COUNT.setText(String.valueOf(BCCount));

                    String strSTCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.STCMSB));
                    String strSTCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.STCLSB));
                    //String strongtea = strSTCMSB + strSTCLSB;
                    //Log.d("blacktea", "::" + strongtea);
                    // TXT_STRONG_TEA_COUNT.setText(strSTCMSB + strSTCLSB);

                    int STCount = (Integer.parseInt(strSTCMSB)<<8)+Integer.parseInt(strSTCLSB);

                    TXT_STRONG_TEA_COUNT.setText(String.valueOf(STCount));


                    String strHMCMSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.HMCMSB));
                    String strHMCLSB = cursor.getString(cursor.getColumnIndex(CoffeeExpressTable.HMCLSB));
                    String hotmilk = strHMCMSB + strHMCLSB;
                    Log.d("blacktea", "::" + hotmilk);

                    int HMCount = (Integer.parseInt(strHMCMSB)<<8)+Integer.parseInt(strHMCLSB);

                    // TXT_HOT_MILK_COUNT.setText(strHMCMSB + strHMCLSB);
                    TXT_HOT_MILK_COUNT.setText(String.valueOf(HMCount));

                }
                while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        /////////////////////



      /*  /////////////////////////////
        String cid;
        String cdate;
        String cmonth;
        String cyear;
        String BCCMSB;
        String BCCLSB;
        String SCCMSB;
        String SCCLSB;
        String LCCMSB;
        String LCCLSB;
        String BTCMSB;
        String BTCLSB;
        String STCMSB;
        String STCLSB;
        String LTCMSB;
        String LTCLSB;
        String HMCMSB;
        String HMCLSB;


        CoffeeExpressTable coffeeTableObj = new CoffeeExpressTable();
        Cursor mCursor = coffeeTableObj.getCursor_mode();
        Log.d("coffee", "VVVVV::");

        if (mCursor != null && mCursor.moveToFirst()) {
            Log.d("coffee", "VVVVV1::");

                cid = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable._ID));
                Log.d("coffee", "VVVVV::" + cid);
                cdate = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.DATE));
                Log.d("coffee", "VVVVV::" + cdate);
                cmonth = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.MONTH));
                Log.d("coffee", "VVVVV::" + cmonth);
                cyear = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.YEAR));
                Log.d("coffee", "VVVVV::" + cyear);
                BCCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BCCMSB));
                Log.d("coffee", "VVVVV::" + BCCMSB);
                BCCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BCCLSB));
                Log.d("coffee", "VVVVV::" + BCCLSB);
                SCCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.SCCMSB));
                Log.d("coffee", "VVVVV::" + SCCMSB);
                SCCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.SCCLSB));
                Log.d("coffee", "VVVVV::" + SCCLSB);
                LCCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.LCCMSB));
                Log.d("coffee", "VVVVV::" + LCCMSB);
                LCCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.LCCLSB));
                Log.d("coffee", "VVVVV::" + LCCLSB);
                BTCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BTCMSB));
                Log.d("coffee", "VVVVV::" + BTCMSB);
                BTCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BTCLSB));
                Log.d("coffee", "VVVVV::" + BTCLSB);
                STCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.STCMSB));
                Log.d("coffee", "VVVVV::" + STCMSB);
                STCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.STCLSB));
                Log.d("coffee", "VVVVV::" + STCLSB);
                LTCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.LTCMSB));
                Log.d("coffee", "VVVVV::" + LTCMSB);
                LTCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.LTCLSB));
                Log.d("coffee", "VVVVV::" + LTCLSB);
                HMCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.HMCMSB));
                Log.d("coffee", "VVVVV::" + HMCMSB);
                HMCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.HMCLSB));
                Log.d("coffee", "VVVVV::" + HMCLSB);

            if(date==cdate && month==cmonth && year==cyear)
            {

                Log.v("Condition","write");
                String strBCCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BTCMSB));
                String strBCCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BTCLSB));
                //  int BCCCount = Integer.getInteger(strBCCMSB)+Integer.getInteger(strBCCLSB);

                String result;
                result=strBCCMSB+strBCCLSB;
                TXT_BLACK_TEA_COUNT.setText(result);
            }

        }


        ///////////////////////////*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_count_log, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
         DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                view.setMaxDate(System.currentTimeMillis());
                updateLabel();
            }
        };

        DatePickerDialog mDatePickerDialog = new DatePickerDialog(CountLog.this,date,myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePickerDialog.show();

       /* new DatePickerDialog(CountLog.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();   */
    }
}

package com.mmm.coffee_machine.activity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.mmm.coffee_machine.R;

import java.util.ArrayList;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private ArrayList<String> moviesList;
    private ArrayList<String> moviesList1;
    private ArrayList<String> moviesList2;
    private ArrayList<String> moviesList3;
    private ArrayList<String> moviesList4;
    private ArrayList<String> moviesList5;
    private ArrayList<String> moviesList6;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView countblacktea, countlighttea, countstrongtea, countblackcoffee,countlightcoffee,countstrongcoffee,countHotmilk;

        public MyViewHolder(View view) {
            super(view);
            countblackcoffee  = (TextView) view.findViewById(R.id.black_coffee_count1);
            countlightcoffee = (TextView) view.findViewById(R.id.light_coffee_count1);
            countstrongcoffee = (TextView) view.findViewById(R.id.strong_coffee_count1);
            countblacktea = (TextView) view.findViewById(R.id.black_tea_count1);
            countlighttea = (TextView) view.findViewById(R.id.light_tea_count1);
            countstrongtea = (TextView) view.findViewById(R.id.strong_tea_count1);
            countHotmilk = (TextView) view.findViewById(R.id.hot_milk_count1);

        }
    }


    public MoviesAdapter(ArrayList<String> moviesList,
                         ArrayList<String> moviesList1,
                         ArrayList<String> moviesList2,
                         ArrayList<String> moviesList3,
                         ArrayList<String> moviesList4,
                         ArrayList<String> moviesList5,
                         ArrayList<String> moviesList6) {
        this.moviesList = moviesList;
        this.moviesList1 = moviesList1;
        this.moviesList2 = moviesList2;
        this.moviesList3 = moviesList3;
        this.moviesList4 = moviesList4;
        this.moviesList5 = moviesList5;
        this.moviesList6 = moviesList6;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cursor_item_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.countblacktea.setText(String.valueOf(moviesList.get(position)));
        holder.countlighttea.setText(String.valueOf(moviesList1.get(position)));
        holder.countstrongtea.setText(String.valueOf(moviesList2.get(position)));
        holder.countblackcoffee.setText(String.valueOf(moviesList3.get(position)));
        holder.countlightcoffee.setText(String.valueOf(moviesList4.get(position)));
        holder.countstrongcoffee.setText(String.valueOf(moviesList5.get(position)));
        holder.countHotmilk.setText(String.valueOf(moviesList6.get(position)));


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}

/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mmm.coffee_machine.BLE;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.mmm.coffee_machine.CoffeeApplication;
import com.mmm.coffee_machine.activity.LogRequest;
import com.mmm.coffee_machine.db.CoffeeExpressTable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeService extends Service {
    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";
    public final static UUID UUID_HEART_RATE_MEASUREMENT =
            UUID.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT);
    private final static String TAG = BluetoothLeService.class.getSimpleName();
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static final UUID GET_SERVICE_UUID = UUID.fromString("0000bbb0-0000-1000-8000-00805f9b34fb");
    private static final UUID GET_DATA_CHAR = UUID.fromString("0000bbb2-0000-1000-8000-00805f9b34fb");
    private static final UUID CONFIG_DESCRIPTOR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static long RECEIVE_BYTE_COUNT = 0;
    public static long RECEIVE_BYTE_COUNT_FINISH = 0;
    private final IBinder mBinder = new LocalBinder();
    BluetoothGattCharacteristic get_characteristic;
    File receiveFile = new File(Environment.getExternalStorageDirectory() + File.separator + "receive.txt");
    FileOutputStream mReceiveFOS = null;
    FileInputStream mReceiveFIS = null;
    byte[] bytes = null;
    private File file;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;
    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.v(TAG, "UUUUU onConnectionStateChange STATE_CONNECTED...");
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);

                SharedPreferences sharedpreferences_ = getSharedPreferences(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedpreferences_.edit();
                editor.clear();
                editor.putString(CoffeeApplication.PREF_DEVICE_NAME, gatt.getDevice().getName());
                editor.putString(CoffeeApplication.PREF_DEVICE_ADDRESS, gatt.getDevice().getAddress());
                editor.commit();

                updateMessageStatus(CoffeeApplication.STATUS_CONNECTED);

                SharedPreferences sharedpreferences = getSharedPreferences(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, CoffeeApplication.STATUS_CONNECTED);
                editor_seq_no.commit();
                //Display Toast after Bluetooth Connection
                //Toast.makeText(getApplicationContext(), "BluetoothProfile STATE_CONNECTED", Toast.LENGTH_SHORT).show();

                mBluetoothGatt.discoverServices();

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;

                broadcastUpdate(intentAction);
                SharedPreferences sharedpreferences_ = getSharedPreferences(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences_.edit();
                editor.clear();
                editor.putString(CoffeeApplication.PREF_DEVICE_NAME, gatt.getDevice().getName());
                editor.putString(CoffeeApplication.PREF_DEVICE_ADDRESS, gatt.getDevice().getAddress());
                editor.commit();

                updateMessageStatus(CoffeeApplication.STATUS_DISCONNECTED);

                SharedPreferences sharedpreferences = getSharedPreferences(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, CoffeeApplication.STATUS_DISCONNECTED);

                editor_seq_no.clear();
                editor_seq_no.commit();

            }
        }

        private void sendMyBroadCastLogStatus(String  logstatus)
        {
            try
            {
                Log.d(TAG,"logstatusset data received if sendbroadcast called");

                Intent broadCastIntent = new Intent();
                broadCastIntent.setAction(LogRequest.BROADCAST_ACTION);

                // uncomment this line if you want to send data
                broadCastIntent.putExtra("data", logstatus);

                sendBroadcast(broadCastIntent);

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                if (mBluetoothGatt != null) {
                    Log.v(TAG, "mConnectedGatt not null");
                    /*check if the service is available on the device*/
                    BluetoothGattService mCustomService = mBluetoothGatt.getService(GET_SERVICE_UUID);
                    if (mCustomService == null) {
                        Log.w(TAG, "Custom BLE Service not found");
                        return;
                    }
        /*get the read characteristic from the service*/
                    BluetoothGattCharacteristic mWriteCharacteristic = mCustomService.getCharacteristic(GET_DATA_CHAR);
                    //Enable local notifications
                    gatt.setCharacteristicNotification(mWriteCharacteristic, true);
                    //Enabled remote notifications
                    BluetoothGattDescriptor desc = mWriteCharacteristic.getDescriptor(CONFIG_DESCRIPTOR);
                    desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    gatt.writeDescriptor(desc);

//        // This is specific to Heart Rate Measurement.
                    BluetoothGattDescriptor descriptor = mWriteCharacteristic.getDescriptor(
                            UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    mBluetoothGatt.writeDescriptor(descriptor);
                }
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }

//            if(CoffeeApplication.FLAG_REQUEST_DEVICE_STATUS == 1) {
//                //send Request for getting the device status
//                Log.v(TAG, "UUUUU CoffeeApplication.FLAG_REQUEST_DEVICE_STATUS == 1 if ");
//                byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x03, 0x01, 0x03, 0x04, 0x05,
//                        0x06, 0x07, 0x08, 0x09, 0x41,
//                        0x56, 0x45, 0x66, 0x6d, 0x74,
//                        0x20, 0x0D, 0x0A};
//                try {
//                    writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
//                    Log.v(TAG, " UUUUU DEVICE COMMUNICATION Write 31... success");
//                    CoffeeApplication.FLAG_REQUEST_DEVICE_STATUS = 0;
//                    // Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.v(TAG, " UUUUU DEVICE COMMUNICATION Write 31... failed");
//                }
//            }

        }

        private void updateMessageStatus(String mMessage) {
            Log.v("QQQQM", " String Message for update : " + mMessage);
        }


        public void sendRTCtime () {


            Log.v(TAG, "UUUU Update RTC Time Event");
            if (DeviceControlActivity.mBluetoothLeService!=null) {
                Log.v(TAG, "UUUU Update RTC Time Event step 2");
                Calendar c = Calendar.getInstance();
                //c.add(Calendar.MONTH, 1);
                int seconds = c.get(Calendar.SECOND);
                int date = c.get(Calendar.DATE);
                int hours = c.get(Calendar.HOUR_OF_DAY);
                int min = c.get(Calendar.MINUTE);
                int month = c.get(Calendar.MONTH)+1;
//                    int year = c.get(Calendar.YEAR);

                int dayofmonth=c.get((Calendar.DAY_OF_WEEK));
                Log.d("Dayofmonth","::"+dayofmonth);

                String thisYear = new SimpleDateFormat("yy").format(new Date());
                int year = Integer.parseInt(thisYear);
                Log.v(TAG, "UUUU Update RTC Time Event step date : "+ date);
                Log.v(TAG, "UUUU Update RTC Time Event step : month : "+ month);
                Log.v(TAG, "UUUU Update RTC Time Event step : year : "+ year);
                Log.v(TAG, "UUUU Update RTC Time Event step : thisYear : "+ Integer.parseInt(thisYear));
                Log.v(TAG, "UUUU Update RTC Time Event step : hours : "+ hours);
                Log.v(TAG, "UUUU Update RTC Time Event step : min : "+ min);
                Log.v(TAG, "UUUU Update RTC Time Event step : se"+ seconds);
                Log.v(TAG, "UUUU Update RTC Time Event step : seconds : "+ seconds);

                byte[] BYTE_SEND_RTC_TIME = {0x41, 0x54, 0x02, 0x09, (byte) year,
                        (byte) month, (byte) date,(byte) hours, (byte) min, (byte) seconds,
                        0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
                        0x20, 0x0D, 0x0A};
                try {
                    DeviceControlActivity.mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);
                    Log.v(TAG, "UUUU Write function 71...");
                    StringBuilder sb = new StringBuilder();
                    for (byte b : BYTE_SEND_RTC_TIME) {
                        if (sb.length() > 0) {
                            sb.append(':');
                        }
                        sb.append(String.format("%02x", b));
                    }






                    Log.v(TAG, "UUUU DEVICE COMMUNICATION ***** Received Value from sb : "+ sb);
                    //Toast.makeText(ctx, "RTC Time Command Send Successfully (Write function 71.)\n"+sb, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                }
            }




        }


        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            Log.v(TAG, "BYTE***  DEVICE COMMUNICATION ***** onCharacteristicChanged");
            //  broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);//
            bytes = characteristic.getValue();

            StringBuilder sb = new StringBuilder();

            for (byte b : bytes) {
                if (sb.length() > 0) {
                    sb.append(':');
                }
                sb.append(String.format("%02x", b));
            }

            Log.v(TAG, "BYTE*** DEVICE COMMUNICATION ***** Received Value from sb1 : "+ sb);

            if (bytes[0] == 0x02 && bytes[1] == 0x08) {

                Log.v(TAG, "BYTE_SEND_LOG_REQUEST_START********RECEIVE_BYTE_COUNT RECEIVE 28.... = " + RECEIVE_BYTE_COUNT + " *********" + RECEIVE_BYTE_COUNT);

                mReConvLength(bytes);

                //send start command
                byte[] BYTE_SEND_LOG_REQUEST_START = {0x41, 0x54, 0x01, 0x06, 0x03, 0x04, 0x05,
                        0x06, 0x07, 0x08, 0x09, 0x41,
                        0x56, 0x45, 0x66, 0x6d, 0x74,
                        0x20, 0x0D, 0x0A};
                try {

                    StringBuilder sb1 = new StringBuilder();

                    for (byte bb : BYTE_SEND_LOG_REQUEST_START) {
                        if (sb1.length() > 0) {
                            sb1.append(':');
                        }
                        sb1.append(String.format("%02x", bb));
                    }

                    Log.v(TAG, "BYTE*** Android userService.getByte(bytes); : " + sb1);
                    writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST_START);
                    Log.v(TAG, " DEVICE COMMUNICATION Write 23...");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            if (bytes[0] == 0x02 && bytes[1] == 0x01) {
                //finished
                Log.v(TAG, " DEVICE COMMUNICATION Receive 25...Completed");
                RECEIVE_BYTE_COUNT = 0;
                RECEIVE_BYTE_COUNT_FINISH = 1;
                /////////////////// File transfer completed

                sendMyBroadCastLogStatus("completed");
                LogRequest.mCreateReceiveFile();


//                    userService.outputFile();
            }


            if (bytes[0] == 0x01 && bytes[1] == 0x07) {
                //write bytes
                Log.v(TAG, " DEVICE COMMUNICATION Receive 17...");
                sendMyBroadCastLogStatus("progress");


                RECEIVE_BYTE_COUNT += 14;
                Log.v(TAG, "********RECEIVE_BYTE_COUNT RECEIVE 17.... = " + RECEIVE_BYTE_COUNT + " *********" + RECEIVE_BYTE_COUNT);
                //       LogRequest.mReceiveByte.setText(""+RECEIVE_BYTE_COUNT);
                try {
                    StringBuilder sb1 = new StringBuilder();

                    for (byte bb : bytes) {
                        if (sb1.length() > 0) {
                            sb1.append(':');
                        }
                        sb1.append(String.format("%02x", bb));
                    }

                    Log.v(TAG, "BYTE*** Android userService.getByte(bytes); : " + sb1);
                    //WRITE BYTES INTO TABLE.
                    int value =0;
                    String dateNew, monthNew, yearNew;

                    int date = bytes[2];
                    int month = bytes[3];
                    int year = bytes[4];

                    if(date<0)
                    {
                        date = date + 256;
                    }
                    date = (value << 8) + (date & 0xff);


                    if(date==1)
                    {
                        dateNew ="01";
                    }
                    else if(date==2)
                    {
                        dateNew ="02";
                    }
                    else if(date==3)
                    {
                        dateNew = "03";
                    }
                    else if(date==4)
                    {
                        dateNew = "04";

                    }else if(date==5)
                    {
                        dateNew = "05";
                    }else if(date==6)
                    {
                        dateNew = "06";
                    }
                    else if(date==7)
                    {
                        dateNew ="07";
                    }
                    else if(date==8)
                    {
                        dateNew ="08";
                    }
                    else if(date==9)
                    {
                        dateNew ="09";
                    }
                    else
                    {
                        dateNew = String.valueOf(date);
                    }


                    if(month<0)
                    {
                        month = month + 256;
                    }

                    month = (value << 8) + (month & 0xff);

                    if(month==1)
                    {
                        monthNew ="01";
                    }
                    else if(month==2)
                    {
                        monthNew ="02";
                    }
                    else if(month==3)
                    {
                        monthNew = "03";
                    }
                    else if(month==4)
                    {
                        monthNew = "04";

                    }else if(month==5)
                    {
                        monthNew = "05";
                    }else if(month==6)
                    {
                        monthNew = "06";
                    }
                    else if(month==7)
                    {
                        monthNew ="07";
                    }
                    else if(month==8)
                    {
                        monthNew ="08";
                    }
                    else if(month==9)
                    {
                        monthNew ="09";
                    }
                    else
                    {
                        monthNew = String.valueOf(month);
                    }


                    if(year<0)
                    {
                        year = year + 256;
                    }

                    StringBuilder yearappend= new StringBuilder();

                    yearappend.append("20");

                    yearappend.append(String.valueOf((value << 8) + (year & 0xff)));



                    String receivedDate=String.valueOf(yearappend)+"-"+monthNew + "-"+ dateNew;

                    Cursor cursor = getApplicationContext().getContentResolver().query(CoffeeExpressTable.CONTENT_URI,
                            null,
                            CoffeeExpressTable.DATE + "=? " ,
                            new String[]{receivedDate},
                            null);


                    Log.d("MMMM","Date 1::"+receivedDate);

                    int bcmsb = bytes[5];
                    int bclsb = bytes[6];
                    if(bclsb<0)
                    {
                        bclsb = bclsb + 256;
                    }

                    if(bcmsb<0)
                    {
                        bcmsb = bcmsb + 256;
                    }


                    int scmsb = bytes[7];
                    int sclsb = bytes[8];
                    if(sclsb<0)
                    {
                        sclsb = sclsb + 256;
                    }

                    if(scmsb<0)
                    {
                        scmsb = scmsb + 256;
                    }


                    int lcmsb = bytes[9];
                    int lclsb = bytes[10];
                    if(lclsb<0)
                    {
                        lclsb = lclsb + 256;
                    }

                    if(scmsb<0)
                    {
                        lcmsb = lcmsb + 256;
                    }

                    int btmsb = bytes[11];
                    int btlsb = bytes[12];
                    if(btlsb<0)
                    {
                        btlsb = btlsb + 256;
                    }

                    if(btmsb<0)
                    {
                        btmsb = btmsb + 256;
                    }

                    int stmsb = bytes[13];
                    int stlsb = bytes[14];
                    if(stlsb<0)
                    {
                        stlsb = stlsb + 256;
                    }

                    if(stmsb<0)
                    {
                        stmsb = stmsb + 256;
                    }


                    int ltmsb = bytes[15];
                    if(ltmsb<0)
                    {
                        ltmsb = ltmsb + 256;
                    }

                    Log.d("MMMM","Black coffee msb"+String.valueOf((value << 8) + (bcmsb & 0xff)));
                    Log.d("MMMM","Black coffee lsb"+String.valueOf((value << 8) + (bclsb & 0xff)));
                    Log.d("MMMM","Strong coffee msb"+String.valueOf((value << 8) + (scmsb & 0xff)));
                    Log.d("MMMM","Strong coffee lsb"+String.valueOf((value << 8) + (sclsb & 0xff)));
                    Log.d("MMMM","Light coffee msb"+String.valueOf((value << 8) + (lcmsb & 0xff)));
                    Log.d("MMMM","Light coffee lsb"+String.valueOf((value << 8) + (lclsb & 0xff)));
                    Log.d("MMMM","Black tea msb"+String.valueOf((value << 8) + (btmsb & 0xff)));
                    Log.d("MMMM","Black tea lsb"+String.valueOf((value << 8) + (btlsb & 0xff)));
                    Log.d("MMMM","Strong tea msb"+String.valueOf((value << 8) + (stmsb & 0xff)));
                    Log.d("MMMM","Strong tea lsb"+String.valueOf((value << 8) + (stlsb & 0xff)));
                    Log.d("MMMM","Light tea msb"+String.valueOf((value << 8) + (ltmsb & 0xff)));



                    if(cursor != null && cursor.moveToFirst()){
                        //insert data

                        ContentValues values = new ContentValues();

                        values.put(CoffeeExpressTable.BCCMSB, String.valueOf((value << 8) + (bcmsb & 0xff)));
                        values.put(CoffeeExpressTable.BCCLSB, String.valueOf((value << 8) + (bclsb & 0xff)));
                        values.put(CoffeeExpressTable.SCCMSB, String.valueOf((value << 8) + (scmsb & 0xff)));
                        values.put(CoffeeExpressTable.SCCLSB, String.valueOf((value << 8) + (sclsb & 0xff)));
                        values.put(CoffeeExpressTable.LCCMSB, String.valueOf((value << 8) + (lcmsb & 0xff)));
                        values.put(CoffeeExpressTable.LCCLSB, String.valueOf((value << 8) + (lclsb & 0xff)));
                        values.put(CoffeeExpressTable.BTCMSB, String.valueOf((value << 8) + (btmsb & 0xff)));
                        values.put(CoffeeExpressTable.BTCLSB, String.valueOf((value << 8) + (btlsb & 0xff)));
                        values.put(CoffeeExpressTable.STCMSB, String.valueOf((value << 8) + (stmsb & 0xff)));
                        values.put(CoffeeExpressTable.STCLSB, String.valueOf((value << 8) + (stlsb & 0xff)));
                        values.put(CoffeeExpressTable.LTCMSB, String.valueOf((value << 8) + (ltmsb & 0xff)));

                        try {
//                            getContentResolver().update(CoffeeExpressTable.CONTENT_URI, values,
//                                    CoffeeExpressTable.DATE + "=? AND "+ CoffeeExpressTable.MONTH + "=? AND "+ CoffeeExpressTable.YEAR
//                                            + "=? ", new String[]{String.valueOf((value << 8) + (bytes[2] & 0xff)),
//                                            String.valueOf((value << 8) + (bytes[3] & 0xff)),
//                                            String.valueOf((value << 8) + (bytes[4] & 0xff))});


                            getContentResolver().update(CoffeeExpressTable.CONTENT_URI, values,
                                    CoffeeExpressTable.DATE + "=? ", new String[]{receivedDate});
                            Log.v(TAG, "Update Success");
                        } catch (Exception e) {
                        }



                    } else {
                        //insert data
                        ContentValues values = new ContentValues();


                        /*String dt=String.valueOf((value << 8) + (bytes[2] & 0xff))+ "/"+
                                String.valueOf((value << 8) + (bytes[3] & 0xff))+ "/"+
                                String.valueOf((value << 8) + (bytes[4] & 0xff));
                        Log.d("DT2","::"+dt);*/


//                        values.put(CoffeeExpressTable.DATE, String.valueOf((value << 8) + "/"+ (bytes[2] & 0xff))+ String.valueOf((value << 8) + "/"+ (bytes[3] & 0xff))+
//                                String.valueOf((value << 8) + (bytes[4] & 0xff)));


                        values.put(CoffeeExpressTable.DATE,receivedDate);




//                       values.put(CoffeeExpressTable.MONTH, String.valueOf((value << 8) + (bytes[3] & 0xff)));
//                       values.put(CoffeeExpressTable.YEAR, String.valueOf((value << 8) + (bytes[4] & 0xff)));
                        values.put(CoffeeExpressTable.BCCMSB, String.valueOf((value << 8) + (bcmsb & 0xff)));
                        values.put(CoffeeExpressTable.BCCLSB, String.valueOf((value << 8) + (bclsb & 0xff)));
                        values.put(CoffeeExpressTable.SCCMSB, String.valueOf((value << 8) + (scmsb & 0xff)));
                        values.put(CoffeeExpressTable.SCCLSB, String.valueOf((value << 8) + (sclsb & 0xff)));
                        values.put(CoffeeExpressTable.LCCMSB, String.valueOf((value << 8) + (lcmsb & 0xff)));
                        values.put(CoffeeExpressTable.LCCLSB, String.valueOf((value << 8) + (lclsb & 0xff)));
                        values.put(CoffeeExpressTable.BTCMSB, String.valueOf((value << 8) + (btmsb & 0xff)));
                        values.put(CoffeeExpressTable.BTCLSB, String.valueOf((value << 8) + (btlsb & 0xff)));
                        values.put(CoffeeExpressTable.STCMSB, String.valueOf((value << 8) + (stmsb & 0xff)));
                        values.put(CoffeeExpressTable.STCLSB, String.valueOf((value << 8) + (stlsb & 0xff)));
                        values.put(CoffeeExpressTable.LTCMSB, String.valueOf((value << 8) + (ltmsb & 0xff)));

                        Uri uri_insert = null;
                        try {
                            uri_insert = getContentResolver().insert(CoffeeExpressTable.CONTENT_URI, values);
                            Log.v(TAG, "Insert Success");
                        } catch (Exception e) {
                        }
                    }
                    cursor.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (bytes[0] == 0x01 && bytes[1] == 0x08) {
                Log.v(TAG, " DEVICE COMMUNICATION Receive 18...");
                sendMyBroadCastLogStatus("progress");

                Log.v(TAG, "********RECEIVE_BYTE_COUNT RECEIVE 18.... = " + RECEIVE_BYTE_COUNT + " *********" + RECEIVE_BYTE_COUNT);
                //       LogRequest.mReceiveByte.setText(""+RECEIVE_BYTE_COUNT);
                try {
                    StringBuilder sb1 = new StringBuilder();

                    for (byte bb : bytes) {
                        if (sb1.length() > 0) {
                            sb1.append(':');
                        }
                        sb1.append(String.format("%02x", bb));
                    }

                    Log.v(TAG, "BYTE*** Android userService.getByte(bytes); : " + sb1);
                    //WRITE BYTES INTO TABLE.
                    int value = 0;



                    int ltlsb = bytes[5];
                    if(ltlsb<0)
                    {
                        ltlsb = ltlsb + 256;
                    }



                    int hmmsb = bytes[6];
                    int hmlsb = bytes[7];
                    if(hmlsb<0)
                    {
                        hmlsb = hmlsb + 256;
                    }

                    if(hmmsb<0)
                    {
                        hmmsb = hmmsb + 256;
                    }


                    int date = bytes[2];
                    int month = bytes[3];
                    int year = bytes[4];


                    if(year<0)
                    {
                        year = year + 256;
                    }
                    String dateNew, monthNew, yearNew;



                    if(date<0)
                    {
                        date = date + 256;
                    }
                    date = (value << 8) + (date & 0xff);


                    if(date==1)
                    {
                        dateNew ="01";
                    }
                    else if(date==2)
                    {
                        dateNew ="02";
                    }
                    else if(date==3)
                    {
                        dateNew = "03";
                    }
                    else if(date==4)
                    {
                        dateNew = "04";

                    }else if(date==5)
                    {
                        dateNew = "05";
                    }else if(date==6)
                    {
                        dateNew = "06";
                    }
                    else if(date==7)
                    {
                        dateNew ="07";
                    }
                    else if(date==8)
                    {
                        dateNew ="08";
                    }
                    else if(date==9)
                    {
                        dateNew ="09";
                    }
                    else
                    {
                        dateNew = String.valueOf(date);
                    }


                    if(month<0)
                    {
                        month = month + 256;
                    }

                    month = (value << 8) + (month & 0xff);

                    if(month==1)
                    {
                        monthNew ="01";
                    }
                    else if(month==2)
                    {
                        monthNew ="02";
                    }
                    else if(month==3)
                    {
                        monthNew = "03";
                    }
                    else if(month==4)
                    {
                        monthNew = "04";

                    }else if(month==5)
                    {
                        monthNew = "05";
                    }else if(month==6)
                    {
                        monthNew = "06";
                    }
                    else if(month==7)
                    {
                        monthNew ="07";
                    }
                    else if(month==8)
                    {
                        monthNew ="08";
                    }
                    else if(month==9)
                    {
                        monthNew ="09";
                    }
                    else
                    {
                        monthNew = String.valueOf(month);
                    }


                    if(year<0)
                    {
                        year = year + 256;
                    }

                    StringBuilder yearappend= new StringBuilder();

                    yearappend.append("20");

                    yearappend.append(String.valueOf((value << 8) + (year & 0xff)));



                    String receivedDate=String.valueOf(yearappend)+"-"+monthNew + "-"+ dateNew;



                    Cursor cursor = getApplicationContext().getContentResolver().query(CoffeeExpressTable.CONTENT_URI,
                            null,
                            CoffeeExpressTable.DATE + "=? " ,
                            new String[]{receivedDate},
                            null);

                    //insert data
                    ContentValues values = new ContentValues();
                    values.put(CoffeeExpressTable.LTCLSB, String.valueOf((value << 8) + (ltlsb & 0xff)));
                    values.put(CoffeeExpressTable.HMCMSB, String.valueOf((value << 8) + (hmmsb & 0xff)));
                    values.put(CoffeeExpressTable.HMCLSB, String.valueOf((value << 8) + (hmlsb & 0xff)));



                  /*  String dt2=String.valueOf((value << 8) + (bytes[2] & 0xff))+ "/"+
                            String.valueOf((value << 8) + (bytes[3] & 0xff))+ "/"+
                            String.valueOf((value << 8) + (bytes[4] & 0xff));
                    Log.d("DT3","::"+dt2);*/


                  if(cursor!=null && cursor.moveToFirst()) {
                      getContentResolver().update(CoffeeExpressTable.CONTENT_URI, values,
                              CoffeeExpressTable.DATE + "=? ", new String[]{receivedDate});
                  }

                  else
                  {
                      getContentResolver().insert(CoffeeExpressTable.CONTENT_URI, values);
                  }






                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (bytes[0] == 0x03 && bytes[1] == 0x02) {
                Log.v(TAG, "UUUUU DEVICE COMMUNICATION onCharacteristicChanged 111 Received ");

                sendMyBroadCast("disable");

            }

            if(bytes[0] == 0x03 && bytes[1] ==0x05)

            {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sendRTCtime();



                Log.d(TAG,"Serial No set data received");
                if(bytes[8]==0x00 && bytes[9]==0x00 && bytes[10]==0x00 &&
                        bytes[11]==0x00 && bytes[12]==0x00 && bytes[13]==0x00)
                {

                    Log.d(TAG,"Serial No set data received if");

                    sendMyBroadCast("0");


                }

                else

                {
                    int temp =0;
                    int serialnoval=0;
                    int serialnostartpos= 8;
                    StringBuilder serialnostring = new StringBuilder();

                    for(int i=0;i<6;i++)
                    {
                        serialnoval = serialnoval * 10;

                        serialnoval += bytes[serialnostartpos];

                        Log.d(TAG,"Serial No set data received else check bytes received"+serialnoval);
                        /*if(bytes[serialnostartpos]<0)
                        {
                            temp = temp +256;
                        }*/
                        serialnostartpos++;
                        //temp =  temp<<8;
                        //serialnoval = serialnoval + temp;

                        // Log.d(TAG,"Serial No set data received else check bytes received 1 $$$"+temp);
                        Log.d(TAG,"Serial No set data received else check bytes received 2 $$$"+serialnoval);

                        serialnostring.append(String.valueOf(serialnoval));

                        serialnoval = 0;



                    }

                    Log.d(TAG,"Serial No set data received else check bytes received ***"+serialnostring);

                    sendMyBroadCast(String.valueOf(serialnostring));

                    DeviceControlActivity.progressDialog.dismiss();

                    Log.d(TAG,"Serial No set data received else");

                }
            }

        }
    };

    public long mReConvLength(byte[] reqLength) {
        //  reqLength[5] = (byte) 0xf4;
        long value = 0;
        for (int i = 4; i < 7; i++) {
            value = (value << 8) + (reqLength[i] & 0xff);
            long ii = reqLength[i] & 0xff;
        }
        Log.v(TAG, "Receivinglength = length = " + value);

        return value;
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }



    private void sendMyBroadCast(String  serialno)
    {
        try
        {
            Log.d(TAG,"Serial No set data received if sendbroadcast called"+serialno);

            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(DeviceControlActivity.BROADCAST_ACTION_SERIAL);

            // uncomment this line if you want to send data
            broadCastIntent.putExtra("data", serialno);

            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }



    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            int flag = characteristic.getProperties();
            int format = -1;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                Log.d(TAG, "Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                Log.d(TAG, "Heart rate format UINT8.");
            }
            final int heartRate = characteristic.getIntValue(format, 1);
            Log.d(TAG, String.format("Received heart rate: %d", heartRate));
            intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
        } else {
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
                intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
            }
        }
        sendBroadcast(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }
        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled        If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        BluetoothGattService mCustomService = mBluetoothGatt.getService(GET_SERVICE_UUID);
        if (mCustomService == null) {
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mReadCharacteristic = mCustomService.getCharacteristic(GET_DATA_CHAR);
        mBluetoothGatt.setCharacteristicNotification(mReadCharacteristic, enabled);

//        // This is specific to Heart Rate Measurement.
//        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
//            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
//                    UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
//            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//            mBluetoothGatt.writeDescriptor(descriptor);
//        }
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public void readCustomCharacteristic() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        // UUID=0000bbb2-0000-1000-8000-00805f9b34fb
        BluetoothGattService mCustomService = mBluetoothGatt.getService(GET_SERVICE_UUID);
        if (mCustomService == null) {
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mReadCharacteristic = mCustomService.getCharacteristic(GET_DATA_CHAR);
        if (mBluetoothGatt.readCharacteristic(mReadCharacteristic) == false) {
            Log.w(TAG, "Failed to read characteristic");
        }
    }

    public void writeCustomCharacteristic(int value) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        BluetoothGattService mCustomService = mBluetoothGatt.getService(GET_SERVICE_UUID);
        if (mCustomService == null) {
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mWriteCharacteristic = mCustomService.getCharacteristic(GET_DATA_CHAR);
        mWriteCharacteristic.setValue(value, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        if (mBluetoothGatt.writeCharacteristic(mWriteCharacteristic) == false) {
            Log.w(TAG, "Failed to write characteristic");
        }
    }

    public void writeCustomCharacteristic(byte[] byte_send_log_request) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/

        BluetoothGattCharacteristic mWriteCharacteristic = mBluetoothGatt.getService(GET_SERVICE_UUID).getCharacteristic(GET_DATA_CHAR);

        mWriteCharacteristic.setValue(byte_send_log_request);
        // get_characteristic.setValue(getUserText);
        mBluetoothGatt.writeCharacteristic(mWriteCharacteristic);
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }
}

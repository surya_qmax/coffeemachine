package com.mmm.coffee_machine;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mmm.coffee_machine.BLE.DeviceControlActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.mmm.coffee_machine.CoffeeApplication.PREF_DEVICE_ADDRESS;
import static com.mmm.coffee_machine.CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS;
import static com.mmm.coffee_machine.CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS_KEY;
import static com.mmm.coffee_machine.CoffeeApplication.PREF_DEVICE_DETAILS;
import static com.mmm.coffee_machine.CoffeeApplication.PREF_DEVICE_NAME;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    //   implements NavigationView.OnNavigationItemSelectedListener {

    public TextView name_device = null, text_device_address, text_device_status;

    private ListView mListView;

    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    public static String FIND_CLASS = "find_class";
    private String sub_class;
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLeDeviceListAdapter.addDevice(device);
                            mLeDeviceListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            };

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        checkAndRequestPermissions();


        name_device = (TextView) findViewById(R.id.name_device);
//        name_device.setText("Device Name :"+Common.DEVICE_NAME);


        text_device_address = (TextView) findViewById(R.id.txt_device_address);
//        text_device_address.setText("Device Address : "+ Common.cDEVICE_ADDRESS);

        text_device_status = (TextView) findViewById(R.id.txt_device_status);
        //   text_device_status.setText("Device Status : "+Common.cDEVICE_STATUS);

        SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
        String device_name = sharedpreferences.getString(PREF_DEVICE_NAME, "");
        String device_address = sharedpreferences.getString(PREF_DEVICE_ADDRESS, "");

        name_device.setText("Device name : " + device_name);
        text_device_address.setText("Device Address : " + device_address);

        SharedPreferences sharedpreferences_ = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        String connect_state = sharedpreferences_.getString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
        //  text_device_status.setText(connect_state);

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
//                R.string.navigation_drawer_open,
//                R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        mListView = (ListView) findViewById(R.id.device_scan_list);
        mHandler = new Handler();
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

//        /////////////////
//
//        ContentValues values = new ContentValues();
//
//
//        String dt= "14/6/17";
//        Log.d("DT2","::"+dt);
//
//
////                        values.put(CoffeeExpressTable.DATE, String.valueOf((value << 8) + "/"+ (bytes[2] & 0xff))+ String.valueOf((value << 8) + "/"+ (bytes[3] & 0xff))+
////                                String.valueOf((value << 8) + (bytes[4] & 0xff)));
//
//
//        values.put(CoffeeExpressTable.DATE,dt);
//
//
////                       values.put(CoffeeExpressTable.MONTH, String.valueOf((value << 8) + (bytes[3] & 0xff)));
////                       values.put(CoffeeExpressTable.YEAR, String.valueOf((value << 8) + (bytes[4] & 0xff)));
//        values.put(CoffeeExpressTable.BCCMSB, "1");
//        values.put(CoffeeExpressTable.BCCLSB, "2");
//        values.put(CoffeeExpressTable.SCCMSB, "3");
//        values.put(CoffeeExpressTable.SCCLSB, "4");
//        values.put(CoffeeExpressTable.LCCMSB, "5");
//        values.put(CoffeeExpressTable.LCCLSB, "6");
//        values.put(CoffeeExpressTable.BTCMSB, "7");
//        values.put(CoffeeExpressTable.BTCLSB, "8");
//        values.put(CoffeeExpressTable.STCMSB, "9");
//        values.put(CoffeeExpressTable.STCLSB, "10");
//        values.put(CoffeeExpressTable.LTCMSB, "11");
//
//        Uri uri_insert = null;
//        try {
//            uri_insert = getContentResolver().insert(CoffeeExpressTable.CONTENT_URI, values);
//            Log.v(TAG, "Insert Success");
//        } catch (Exception e) {
//        }
//
//
//        ContentValues values1 = new ContentValues();
//        values1.put(CoffeeExpressTable.LTCLSB, "12");
//        values1.put(CoffeeExpressTable.HMCMSB, "13");
//        values1.put(CoffeeExpressTable.HMCLSB, 14);
//
//
//        String dt2="14/6/17";
//        Log.d("DT3","::"+dt2);
//
//        getContentResolver().update(CoffeeExpressTable.CONTENT_URI, values1,
//                CoffeeExpressTable.DATE + "=? ", new String[]{dt2});
//
//        //////////////////
//
//
//        ContentValues values2 = new ContentValues();
//
//
//        String dt3= "15/06/17";
//        Log.d("DT2","::"+dt);
//
//
////                        values.put(CoffeeExpressTable.DATE, String.valueOf((value << 8) + "/"+ (bytes[2] & 0xff))+ String.valueOf((value << 8) + "/"+ (bytes[3] & 0xff))+
////                                String.valueOf((value << 8) + (bytes[4] & 0xff)));
//
//
//        values2.put(CoffeeExpressTable.DATE,dt3);
//
//
////      values.put(CoffeeExpressTable.MONTH, String.valueOf((value << 8) + (bytes[3] & 0xff)));
////      values.put(CoffeeExpressTable.YEAR, String.valueOf((value << 8) + (bytes[4] & 0xff)));
//        values2.put(CoffeeExpressTable.BCCMSB, "13");
//        values2.put(CoffeeExpressTable.BCCLSB, "2");
//        values2.put(CoffeeExpressTable.SCCMSB, "3");
//        values2.put(CoffeeExpressTable.SCCLSB, "4");
//        values2.put(CoffeeExpressTable.LCCMSB, "5");
//        values2.put(CoffeeExpressTable.LCCLSB, "6");
//        values2.put(CoffeeExpressTable.BTCMSB, "7");
//        values2.put(CoffeeExpressTable.BTCLSB, "8");
//        values2.put(CoffeeExpressTable.STCMSB, "9");
//        values2.put(CoffeeExpressTable.STCLSB, "10");
//        values2.put(CoffeeExpressTable.LTCMSB, "11");
//
//        Uri uri_insert1 = null;
//        try {
//            uri_insert1 = getContentResolver().insert(CoffeeExpressTable.CONTENT_URI, values2);
//            Log.v(TAG, "Insert Success");
//        } catch (Exception e) {
//        }
//
//
//        ContentValues values3 = new ContentValues();
//        values3.put(CoffeeExpressTable.LTCLSB, "12");
//        values3.put(CoffeeExpressTable.HMCMSB, "13");
//        values3.put(CoffeeExpressTable.HMCLSB, 14);
//
//
//        String dt4="15/6/17";
//        Log.d("DT3","::"+dt4);
//
//        getContentResolver().update(CoffeeExpressTable.CONTENT_URI, values3,
//                CoffeeExpressTable.DATE + "=? ", new String[]{dt4});
//
//        /////////////////
//
//        String date;
//        String BCCMSB;
//        String BCCLSB;
//        String SCCMSB;
//        String SCCLSB;
//        String LCCMSB;
//        String LCCLSB;
//        String BTCMSB;
//        String BTCLSB;
//        String STCMSB;
//        String STCLSB;
//        String LTCMSB;
//        String LTCLSB;
//        String HMCMSB;
//        String HMCLSB;
//
//
//        CoffeeExpressTable coffeeTableObj = new CoffeeExpressTable();
//        Cursor mCursor = coffeeTableObj.getCursor_mode();
//        Log.d("coffee", "VVVVV::");
//
//        if (mCursor != null && mCursor.moveToFirst()) {
//            Log.d("coffee", "VVVVV1::");
//            do {
//                date = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.DATE));
//                Log.d("coffee", "VVVVV::" + date);
//                BCCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BCCMSB));
//                Log.d("coffee", "VVVVV::" + BCCMSB);
//                BCCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BCCLSB));
//                Log.d("coffee", "VVVVV::" + BCCLSB);
//                SCCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.SCCMSB));
//                Log.d("coffee", "VVVVV::" + SCCMSB);
//                SCCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.SCCLSB));
//                Log.d("coffee", "VVVVV::" + SCCLSB);
//                LCCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.LCCMSB));
//                Log.d("coffee", "VVVVV::" + LCCMSB);
//                LCCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.LCCLSB));
//                Log.d("coffee", "VVVVV::" + LCCLSB);
//                BTCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BTCMSB));
//                Log.d("coffee", "VVVVV::" + BTCMSB);
//                BTCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.BTCLSB));
//                Log.d("coffee", "VVVVV::" + BTCLSB);
//                STCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.STCMSB));
//                Log.d("coffee", "VVVVV::" + STCMSB);
//                STCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.STCLSB));
//                Log.d("coffee", "VVVVV::" + STCLSB);
//                LTCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.LTCMSB));
//                Log.d("coffee", "VVVVV::" + LTCMSB);
//                LTCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.LTCLSB));
//                Log.d("coffee", "VVVVV::" + LTCLSB);
//                HMCMSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.HMCMSB));
//                Log.d("coffee", "VVVVV::" + HMCMSB);
//                HMCLSB = mCursor.getString(mCursor.getColumnIndex(CoffeeExpressTable.HMCLSB));
//                Log.d("coffee", "VVVVV::" + HMCLSB);
//            }
//            while (mCursor.moveToNext());
//            mCursor.close();
//        }
//        /////////////////
    }

    private boolean checkAndRequestPermissions() {
        int storage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int Access_coarse = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int Access_fine = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        int Bluetooth_pre = ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_PRIVILEGED);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (Access_fine != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (Access_coarse != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (Bluetooth_pre != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.BLUETOOTH_PRIVILEGED);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //scanLeDevice(false);
        mLeDeviceListAdapter.clear();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        invalidateOptionsMenu();
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        SharedPreferences sharedpreferences = getSharedPreferences(CoffeeApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
//        editor_seq_no.clear();
//        editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_NAME, "");
//        editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_ADDRESS, "");
//        editor_seq_no.commit();
//
//        SharedPreferences device_status = getSharedPreferences(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor_ = device_status.edit();
//        editor_.clear();
//        editor_.putString(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, "");
//        editor_.commit();
//    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        scanLeDevice(true);

        // Initializes list view adapter.
        mLeDeviceListAdapter = new MainActivity.LeDeviceListAdapter();
        mListView.setAdapter(mLeDeviceListAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
                if (device == null) return;
                Intent intent = null;


                intent = new Intent(getApplicationContext(), DeviceControlActivity.class);

                Log.v(TAG, "sssss0 mDeviceName :" + device.getName() + "mDeviceAddress : " + device.getAddress());
                SharedPreferences sharedpreferences = getSharedPreferences(CoffeeApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_NAME, device.getName());
                editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_ADDRESS, device.getAddress());
                editor_seq_no.commit();
                SharedPreferences sharedpreferences1 = getSharedPreferences(CoffeeApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String mDeviceName = sharedpreferences1.getString(CoffeeApplication.PREF_DEVICE_NAME, "");
                String mDeviceAddress = sharedpreferences1.getString(CoffeeApplication.PREF_DEVICE_ADDRESS, "");
                Log.v(TAG, "sssss mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
                if (mScanning) {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    mScanning = false;
                }
                startActivity(intent);
            }
        });

        final TextView name_device = (TextView) findViewById(R.id.name_device);
        //  name_device.setText("Device Name : "+Common.DEVICE_NAME);
        final TextView text_device_address = (TextView) findViewById(R.id.txt_device_address);
        //text_device_address.setText("Device Address : "+ Common.cDEVICE_ADDRESS);

        final TextView text_device_status = (TextView) findViewById(R.id.txt_device_status);
//        text_device_status.setText("Device Status : "+Common.cDEVICE_STATUS);

        SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
        String device_name = sharedpreferences.getString(PREF_DEVICE_NAME, "");
        String device_address = sharedpreferences.getString(PREF_DEVICE_ADDRESS, "");

        name_device.setText("Device name : " + device_name);
        text_device_address.setText("Device Address : " + device_address);

        SharedPreferences sharedpreferences_ = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        String connect_state = sharedpreferences_.getString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
        //   text_device_status.setText(connect_state);

        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                System.out.println("After 2 secs");

                SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String device_name = sharedpreferences.getString(PREF_DEVICE_NAME, "");
                String device_address = sharedpreferences.getString(PREF_DEVICE_ADDRESS, "");
                name_device.setText("Device name : " + device_name);
                text_device_address.setText("Device Address : " + device_address);

                SharedPreferences sharedpreferences_ = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                String connect_state = sharedpreferences_.getString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
                text_device_status.setText(connect_state);
                handler.removeCallbacks(this);
                handler.postDelayed(this, 10000);
            }
        };
        handler.postDelayed(runnable, 10000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_home) {
//            // Handle the camera action
//            Intent i = new Intent(getApplicationContext(), DeviceScanActivity.class);
//            i.putExtra(DeviceScanActivity.FIND_CLASS, "home");
//            startActivity(i);
//        } else if (id == R.id.nav_switch) {
//            Intent i = new Intent(getApplicationContext(), DeviceScanActivity.class);
//            i.putExtra(DeviceScanActivity.FIND_CLASS, "switch");
//            startActivity(i);
//        } else if (id == R.id.nav_log_request) {
//            Intent i = new Intent(getApplicationContext(), DeviceScanActivity.class);
//            i.putExtra(DeviceScanActivity.FIND_CLASS, "log_request");
//            startActivity(i);
//        } else if (id == R.id.nav_manage) {
//            Intent i = new Intent(getApplicationContext(), MobileDetails.class);
//            startActivity(i);
//        } else if (id == R.id.nav_mode) {
//            Intent i = new Intent(getApplicationContext(), ModeActivity.class);
//            startActivity(i);
//        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    public void updateMessageStatus(String mMessage) {
        Log.v("QQQQM", " String Message for update : " + mMessage);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedpreferences = getSharedPreferences(CoffeeApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
        editor_seq_no.clear();
        editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_NAME, "");
        editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_ADDRESS, "");
        editor_seq_no.commit();

        SharedPreferences device_status = getSharedPreferences(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = device_status.edit();
        editor_.clear();
        editor_.putString(CoffeeApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, "");
        editor_.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                mLeDeviceListAdapter.clear();
                scanLeDevice(true);
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
        Button mConnect;
    }

    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = MainActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            MainActivity.ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new MainActivity.ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.mConnect = (Button) view.findViewById(R.id.btn_connect);
                view.setTag(viewHolder);
            } else {
                viewHolder = (MainActivity.ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            viewHolder.deviceAddress.setText(device.getAddress());

            viewHolder.mConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final BluetoothDevice device = mLeDeviceListAdapter.getDevice(i);
                    if (device == null) return;
                    Intent intent = null;

                    intent = new Intent(getApplicationContext(), DeviceControlActivity.class);
                    Log.v(TAG, "UUUUU mConnect CoffeeApplication.FLAG_REQUEST_DEVICE_STATUS = 1.");
                    CoffeeApplication.FLAG_REQUEST_DEVICE_STATUS = 1;
                    Log.v(TAG, "sssss0 mDeviceName :" + device.getName() + "mDeviceAddress : " + device.getAddress());
                    SharedPreferences sharedpreferences = getSharedPreferences(CoffeeApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                    editor_seq_no.clear();
                    editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_NAME, device.getName());
                    editor_seq_no.putString(CoffeeApplication.PREF_DEVICE_ADDRESS, device.getAddress());
                    editor_seq_no.commit();
                    SharedPreferences sharedpreferences1 = getSharedPreferences(CoffeeApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                    String mDeviceName = sharedpreferences1.getString(CoffeeApplication.PREF_DEVICE_NAME, "");
                    String mDeviceAddress = sharedpreferences1.getString(CoffeeApplication.PREF_DEVICE_ADDRESS, "");
                    Log.v(TAG, "sssss mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);
                    intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
                    intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
//                    if (mScanning) {
//                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
//                        mScanning = false;
//                    }
                    startActivity(intent);
                }
            });
            return view;
        }
    }
}